# SWIRD

Short-wave infrared detection (SWIRD) is a technology that uses light to automatically identify and detect objects.


## Visuals
### Device used as a camera

<img src="Visuals/detector_without_housing.jpg"  width="259" height="194">
<img src="Visuals/IMG_I.png"  width="321" height="254">
<img src="Visuals/IMG_II.png"  width="321" height="254" alt="TEST">


### Device used as a detector
{+ Checkout the Visuals folder for artefact (due to compression) free *.mp4 videos +}

![Bumblebee flight from hive to feeder in the flight arena *](Visuals/HiveToFeederC.mp4)![flight arena](Visuals/freefly.jpg "* flight arena")















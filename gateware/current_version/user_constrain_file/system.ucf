# Constraints for FlouTrack Board

                                                                          
#    Copyright (C) 2019 Authors                                               
#                                                                          
#    This file is part of FlouTrack.
#
#    FlouTrack is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    FlouTrack is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License.
#    If not, see <http://www.gnu.org/licenses/>.
                                  

NET "sys_clk" LOC = AA12;
NET "sys_clk" IOSTANDARD = LVCMOS33;

## System level constraints
NET "sys_clk" TNM_NET = "sys_clk";
TIMESPEC TS_sys_clk = PERIOD "sys_clk" 8 ns;

# LED 4x

#NET "led[0]" LOC = H8;
#NET "led[0]" IOSTANDARD = LVCMOS15;
#NET "led[1]" LOC = J7;
#NET "led[1]" IOSTANDARD = LVCMOS15;
#NET "led[2]" LOC = T4;
#NET "led[2]" IOSTANDARD = LVCMOS15;
#NET "led[3]" LOC = T3;
#NET "led[3]" IOSTANDARD = LVCMOS15;

#### Module GMII - Ethernet constraints

NET "tx_data[7]" LOC = AA14;
NET "tx_data[7]" IOSTANDARD = LVCMOS33;
NET "tx_data[6]" LOC = V15;
NET "tx_data[6]" IOSTANDARD = LVCMOS33;
NET "tx_data[5]" LOC = Y14;
NET "tx_data[5]" IOSTANDARD = LVCMOS33;
NET "tx_data[4]" LOC = T16;
NET "tx_data[4]" IOSTANDARD = LVCMOS33;
NET "tx_data[3]" LOC = W14;
NET "tx_data[3]" IOSTANDARD = LVCMOS33;
NET "tx_data[2]" LOC = AA16;
NET "tx_data[2]" IOSTANDARD = LVCMOS33;
NET "tx_data[1]" LOC = AB14;
NET "tx_data[1]" IOSTANDARD = LVCMOS33;
NET "tx_data[0]" LOC = AA18;
NET "tx_data[0]" IOSTANDARD = LVCMOS33;
NET "tx_en" LOC = AB16;
NET "tx_en" IOSTANDARD = LVCMOS33;
NET "tx_er" LOC = AB18;
NET "tx_er" IOSTANDARD = LVCMOS33;
NET "tx_clk" LOC = R11;
NET "tx_clk" IOSTANDARD = LVCMOS33;
NET "rx_data[7]" LOC = U6;
NET "rx_data[7]" IOSTANDARD = LVCMOS33;
NET "rx_data[6]" LOC = W9;
NET "rx_data[6]" IOSTANDARD = LVCMOS33;
NET "rx_data[5]" LOC = V5;
NET "rx_data[5]" IOSTANDARD = LVCMOS33;
NET "rx_data[4]" LOC = V7;
NET "rx_data[4]" IOSTANDARD = LVCMOS33;
NET "rx_data[3]" LOC = U9;
NET "rx_data[3]" IOSTANDARD = LVCMOS33;
NET "rx_data[2]" LOC = W4;
NET "rx_data[2]" IOSTANDARD = LVCMOS33;
NET "rx_data[1]" LOC = W8;
NET "rx_data[1]" IOSTANDARD = LVCMOS33;
NET "rx_data[0]" LOC = Y3;
NET "rx_data[0]" IOSTANDARD = LVCMOS33;
NET "rx_dv" LOC = Y4;
NET "rx_dv" IOSTANDARD = LVCMOS33;
NET "rx_er" LOC = Y8;
NET "rx_er" IOSTANDARD = LVCMOS33;
NET "rx_clk" LOC = Y11;
NET "rx_clk" IOSTANDARD = LVCMOS33;
NET "rstn" TIG;
NET "rstn" LOC = T15;
NET "rstn" IOSTANDARD = LVCMOS33;

# Clock domain crossing
#NET "*/xst_fifo_generator/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.gcx.clkx/wr_pntr_gc<*>" TIG;
#NET "*/xst_fifo_generator/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.gcx.clkx/rd_pntr_gc<*>" TIG;
#NET "rst_inst/done" TIG;
NET "Inst_rst/done" TIG;
# SPI from C to AFE
#NET "sclk_uC" LOC = AB4;
NET "sclk_uC" LOC = AB8;
NET "sclk_uC" IOSTANDARD = LVCMOS33;
#NET "sen_uC" LOC = AA4;
NET "sen_uC" LOC = AA6;
NET "sen_uC" IOSTANDARD = LVCMOS33;
#NET "sdi_uC" LOC = AB6;
NET "sdi_uC" LOC = AB7;
NET "sdi_uC" IOSTANDARD = LVCMOS33;
#NET "sdo_uC" LOC = AA6;
NET "sdo_uC" LOC = Y7;
NET "sdo_uC" IOSTANDARD = LVCMOS33;

# AFE Interface
NET "sclk_AFE" LOC = U17;
NET "sclk_AFE" IOSTANDARD = LVCMOS33;
NET "sclk_AFE" PULLDOWN;
NET "sen_AFE" LOC = W13;
NET "sen_AFE" IOSTANDARD = LVCMOS33;
NET "sen_AFE" PULLUP;
NET "sdi_AFE" LOC = U16;
NET "sdi_AFE" IOSTANDARD = LVCMOS33;
NET "sdi_AFE" PULLDOWN;
#R9;#GPIO2      T18;# GPIO1
NET "sdo_AFE" LOC = T18;
NET "sdo_AFE" IOSTANDARD = LVCMOS33;
NET "sdo_AFE" PULLDOWN;

NET "AFE_XSH2" LOC = AB11;
NET "AFE_XSH2" IOSTANDARD = LVCMOS33;
NET "AFE_XSH2" PULLDOWN;
NET "AFE_XSH3" LOC = T7;
NET "AFE_XSH3" IOSTANDARD = LVCMOS33;
NET "AFE_XSH3" PULLDOWN;
NET "AFE_XSH4" LOC = R7;
NET "AFE_XSH4" IOSTANDARD = LVCMOS33;
NET "AFE_XSH4" PULLDOWN;
NET "AFE_GPIO0" LOC = R8;
NET "AFE_GPIO0" IOSTANDARD = LVCMOS33;
NET "AFE_GPIO0" PULLDOWN;
NET "AFE_GPIO2" LOC = R9;
NET "AFE_GPIO2" IOSTANDARD = LVCMOS33;
NET "AFE_GPIO2" PULLDOWN;
NET "RCLKN" LOC = W11;
NET "RCLKN" IOSTANDARD = LVCMOS33;
NET "RCLKN" PULLDOWN;

NET "RCLKP" LOC = V11;
NET "RCLKP" IOSTANDARD = LVCMOS33;

NET "AFE_Data[7]" LOC = V17;
NET "AFE_Data[7]" IOSTANDARD = LVCMOS33;
NET "AFE_Data[6]" LOC = W17;
NET "AFE_Data[6]" IOSTANDARD = LVCMOS33;
NET "AFE_Data[5]" LOC = W18;
NET "AFE_Data[5]" IOSTANDARD = LVCMOS33;
NET "AFE_Data[4]" LOC = Y18;
NET "AFE_Data[4]" IOSTANDARD = LVCMOS33;
NET "AFE_Data[3]" LOC = Y19;
NET "AFE_Data[3]" IOSTANDARD = LVCMOS33;
NET "AFE_Data[2]" LOC = AB19;
NET "AFE_Data[2]" IOSTANDARD = LVCMOS33;
NET "AFE_Data[1]" LOC = V18;
NET "AFE_Data[1]" IOSTANDARD = LVCMOS33;
NET "AFE_Data[0]" LOC = V19;
NET "AFE_Data[0]" IOSTANDARD = LVCMOS33;

NET "AFE_LSYNC" LOC = T17;
NET "AFE_LSYNC" IOSTANDARD = LVCMOS33;
#NET "AFE_LSYNC" PULLDOWN;
NET "AFE_XST" LOC = Y12;
NET "AFE_XST" IOSTANDARD = LVCMOS33;
NET "AFE_XST" PULLDOWN;
NET "AFE_XCLR" LOC = R15;
NET "AFE_XCLR" IOSTANDARD = LVCMOS33;
NET "AFE_XCLR" PULLDOWN;
NET "AFE_XP1" LOC = R16;
NET "AFE_XP1" IOSTANDARD = LVCMOS33;
NET "AFE_XP1" PULLDOWN;
NET "AFE_XP2" LOC = AB21;
NET "AFE_XP2" IOSTANDARD = LVCMOS33;
NET "AFE_XP2" PULLDOWN;
NET "AFE_XP3" LOC = AA21;
NET "AFE_XP3" IOSTANDARD = LVCMOS33;
NET "AFE_XP3" PULLDOWN;

NET "AFE_XRS" LOC = Y15;
NET "AFE_XRS" IOSTANDARD = LVCMOS33;
NET "AFE_XRS" PULLDOWN;
NET "AFE_XCP" LOC = AB15;
NET "AFE_XCP" IOSTANDARD = LVCMOS33;
NET "AFE_XCP" PULLDOWN;
NET "AFE_X2L" LOC = Y17;
NET "AFE_X2L" IOSTANDARD = LVCMOS33;
NET "AFE_X2L" PULLDOWN;
NET "AFE_X1L" LOC = AB17;
NET "AFE_X1L" IOSTANDARD = LVCMOS33;
NET "AFE_X1L" PULLDOWN;
NET "AFE_XP4" LOC = AB12;
NET "AFE_XP4" IOSTANDARD = LVCMOS33;
NET "AFE_XP4" PULLDOWN;

# Button Interface from C
NET "uC_button_upLeft" LOC = C6;
NET "uC_button_upLeft" IOSTANDARD = LVCMOS33;
NET "uC_button_upLeft" PULLDOWN;
NET "uC_button_upLeft" IN_TERM = UNTUNED_SPLIT_50;
NET "uC_button_upRight" LOC = D6;
NET "uC_button_upRight" IOSTANDARD = LVCMOS33;
NET "uC_button_upRight" PULLDOWN;
NET "uC_button_upRight" IN_TERM = UNTUNED_SPLIT_50;
NET "uC_button_downLeft" LOC =  D8;
NET "uC_button_downLeft" IOSTANDARD = LVCMOS33;
NET "uC_button_downLeft" PULLDOWN;
NET "uC_button_downLeft" IN_TERM = UNTUNED_SPLIT_50;
NET "uC_button_downRight" LOC = D7;
NET "uC_button_downRight" IOSTANDARD = LVCMOS33;
NET "uC_button_downRight" PULLDOWN;
NET "uC_button_downRight" IN_TERM = UNTUNED_SPLIT_50;

# Servo Interface
NET "servo_1" LOC = D17;
NET "servo_1" IOSTANDARD = LVCMOS33;
NET "servo_1" PULLDOWN;
NET "servo_2" LOC = E16;
NET "servo_2" IOSTANDARD = LVCMOS33;
NET "servo_2" PULLDOWN;
NET "servo_3" LOC = C16;
NET "servo_3" IOSTANDARD = LVCMOS33;
NET "servo_3" PULLDOWN;
NET "servo_4" LOC = D15;
NET "servo_4" IOSTANDARD = LVCMOS33;
NET "servo_4" PULLDOWN;

# Testpads
NET "tp2" LOC = AA10;
NET "tp2" IOSTANDARD = LVCMOS33;
NET "tp12" LOC = AB10;
NET "tp12" IOSTANDARD = LVCMOS33;
NET "tp13" LOC = Y9;
NET "tp13" IOSTANDARD = LVCMOS33;
NET "tp14" LOC = AB9;
NET "tp14" IOSTANDARD = LVCMOS33;
NET "tp15" LOC = AA8;
NET "tp15" IOSTANDARD = LVCMOS33;


#Sensor
NET "S_FSYNC" LOC = A12;
NET "S_FSYNC" IOSTANDARD = LVCMOS33;
NET "S_LSYNC" LOC = C13;
NET "S_LSYNC" IOSTANDARD = LVCMOS33;
NET "S_LSYNC" DRIVE = 2;
NET "S_LSYNC" SLEW = slow;
NET "S_FIELD" LOC = C11;
NET "S_FIELD" IOSTANDARD = LVCMOS33;
NET "S_RST" LOC = A14;
NET "S_RST" IOSTANDARD = LVCMOS33;
NET "S_RST" PULLDOWN;
NET "S_CLK" LOC = B14;
NET "S_CLK" IOSTANDARD = LVCMOS33;
NET "S_CLK" DRIVE = 24;
NET "S_CLK" SLEW = slow;
NET "S_DATA" LOC = A11;
NET "S_DATA" IOSTANDARD = LVCMOS33;
# level shifter
NET "LS_ENABLE" LOC = A13;
NET "LS_ENABLE" IOSTANDARD = LVCMOS33;

# This signal is sent from the FPGA to the uC to indicate, the uC can power the sensor.
NET "sensor_can_be_powered" LOC = B18;
NET "sensor_can_be_powered" IOSTANDARD = LVCMOS33;
NET "sensor_can_be_powered" PULLDOWN;

# This signal is sent by the uC and indicates the FPGA, he can release the reset of the sensor.
NET "uC_sensor_power_enable_indicator" LOC = A17;
NET "uC_sensor_power_enable_indicator" IOSTANDARD = LVCMOS33;
NET "uC_sensor_power_enable_indicator" PULLDOWN;

# UART Connection between FPGA and uC (USART 3, PC10(TX), PC11(RX))
NET "uC_UART_Tx" LOC = C17;
NET "uC_UART_Tx" IOSTANDARD = LVCMOS33;
NET "uC_UART_RX" LOC = A18;
NET "uC_UART_RX" IOSTANDARD = LVCMOS33;


NET "sclk_AFE" DRIVE = 2;
NET "sdi_AFE" DRIVE = 2;
NET "sdo_uC" DRIVE = 2;
NET "sen_AFE" DRIVE = 2;
NET "RCLKP" DRIVE = 12;


NET "sen_AFE" SLEW = SLOW;
NET "sdo_uC" SLEW = SLOW;
NET "sdi_AFE" SLEW = SLOW;
NET "sclk_AFE" SLEW = SLOW;
NET "RCLKP" SLEW = FAST;


#NET "testPin" OUT_TERM = UNTUNED_50;


NET "tp2" DRIVE = 4;
NET "tp12" DRIVE = 2;
NET "tp13" DRIVE = 2;
NET "tp14" DRIVE = 4;
NET "tp15" DRIVE = 2;
NET "tp15" SLEW = SLOW;
NET "tp14" SLEW = SLOW;
NET "tp13" SLEW = SLOW;
NET "tp12" SLEW = SLOW;
NET "tp2" SLEW = SLOW;


NET "AFE_Data[0]" IN_TERM = UNTUNED_SPLIT_50;
NET "AFE_Data[1]" IN_TERM = UNTUNED_SPLIT_50;
NET "AFE_Data[2]" IN_TERM = UNTUNED_SPLIT_50;
NET "AFE_Data[3]" IN_TERM = UNTUNED_SPLIT_50;
NET "AFE_Data[4]" IN_TERM = UNTUNED_SPLIT_50;
NET "AFE_Data[5]" IN_TERM = UNTUNED_SPLIT_50;
NET "AFE_Data[6]" IN_TERM = UNTUNED_SPLIT_50;
NET "AFE_Data[7]" IN_TERM = UNTUNED_SPLIT_50;

# PlanAhead Generated IO constraints 



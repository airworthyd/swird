----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    09:13:06 02/20/2019 
-- Design Name: 
-- Module Name:    Fifo_Multiplexer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                                                                      --
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Fifo_Read_Multiplexer is
    Port ( 
				clk : in STD_LOGIC;
				selectReadFIFO : in STD_LOGIC;
				readReq : in  STD_LOGIC;
				dataOut : out  STD_LOGIC_VECTOR (7 downto 0);
				empty : out  STD_LOGIC;
				rdUsedW : out  STD_LOGIC_VECTOR (9 downto 0);
				readReq_0 : out  STD_LOGIC;
				readReq_1 : out  STD_LOGIC;
				dataOut_0 : in  STD_LOGIC_VECTOR (7 downto 0);
				dataOut_1 : in  STD_LOGIC_VECTOR (7 downto 0);
				empty_0 : in  STD_LOGIC;
				empty_1 : in  STD_LOGIC;
				rdUsedW_0 : in  STD_LOGIC_VECTOR (9 downto 0);
				rdUsedW_1 : in  STD_LOGIC_VECTOR (9 downto 0));
end Fifo_Read_Multiplexer;

architecture Behavioral of Fifo_Read_Multiplexer is

begin

	   Process (clk,selectReadFIFO)
   Begin
 	  -- If clk'event and clk = '1' Then
			If selectReadFIFO = '0' Then
				readReq_0 	<= readReq;	
				dataOut 		<= dataOut_0;
				empty 		<= empty_0;
				rdUsedW 		<= rdUsedW_0;
				readReq_1 	<= '0';
			Else
				readReq_1 	<= readReq;
				dataOut 		<= dataOut_1;
				empty 		<= empty_1;
				rdUsedW 		<= rdUsedW_1;
				readReq_0 	<= '0';
			End If;
		--end if;
	end process;
	
end Behavioral;


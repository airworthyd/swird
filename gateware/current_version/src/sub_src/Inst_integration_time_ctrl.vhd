----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    09:16:41 09/16/2019 
-- Design Name: 
-- Module Name:    Inst_integration_time_ctrl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
-- The sensor integration time (= exposure time) is equal to
-- the low time of the sensor_fsync_signal. (See module sensor_ctrl.vhd)
-- The parameter 'integration_time_output' must be multiplied with the image line time
-- to obtain the integration time in ms. The image line time depends on the frame rate.
-- The relation between line time and framerate (images per second) is:
--  		line time = 1 / (framerate [fps] * 512)
--			exposure time [s] = 'integration_time_output' * line time 
--			example: line time @ 100fps = 19,5us
--						'integration_time_output' = 100
--						exposure time = 19,5us * 100 = 1,95ms	
--		integration_time_output = integration_time_input + integration_time_signal
--		integration_time_signal can be manipulated with buttons
--  	integration_time_input comes from the UART interface
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Integration_time_ctrl is
    Port ( 	rst : in  STD_LOGIC;
				clk : in  STD_LOGIC;
				up : in  STD_LOGIC;
				down : in  STD_LOGIC;
				integration_time_input : in  STD_LOGIC_VECTOR (7 downto 0):=x"00";
				integration_time_output : out  STD_LOGIC_VECTOR (8 downto 0));
end Integration_time_ctrl;

architecture Behavioral of Integration_time_ctrl is

signal integration_time_signal : integer range -127 to 127 := 0;
signal integration_time_signal_upper_limit : integer range 0 to 127 := 120;
signal integration_time_signal_lower_limit : integer range -127 to 0 := 0;
signal last_up : std_logic := '0';
signal last_down : std_logic := '0';
signal debounce : integer range 0 to 50000 := 0;
signal int_integration_time_signal : integer range 0 to 512 := 1;

begin

	Process(clk)
	begin
	IF (rising_edge(clk)) then 
		If RST = '0' Then
			
			
			IF ((up = '1') and (last_up = '0') and (integration_time_signal < 125) and (debounce = 0))  THEN  
				last_up <= '1';		
					integration_time_signal <= integration_time_signal + 1;
				debounce <= 50000;
			end if;
			IF ((down = '1') and (last_down = '0') and (integration_time_signal > -125) and (debounce = 0))  THEN 
				last_down <= '1';
				integration_time_signal <= integration_time_signal - 1;
				debounce <= 50000;
			end if;
			
			IF ((up = '0') and (last_up = '1') and (debounce = 0))  THEN
				last_up <= '0';
				debounce <= 50000;
			end if;
			IF ((down = '0') and (last_down = '1') and (debounce = 0))  THEN
				last_down <= '0';
				debounce <= 50000;
			end if;
			
			integration_time_signal_upper_limit <= 127 - to_integer(signed(integration_time_input));
			integration_time_signal_lower_limit <= - to_integer(signed(integration_time_input));
			if (integration_time_signal > integration_time_signal_upper_limit) then
				integration_time_signal <= integration_time_signal_upper_limit;
			end if;
			if(integration_time_signal < integration_time_signal_lower_limit)then
				integration_time_signal <= integration_time_signal_lower_limit;
			end if;
			int_integration_time_signal <= 4*(integration_time_signal + to_integer(signed(integration_time_input)));
			integration_time_output <= std_logic_vector(to_unsigned(int_integration_time_signal, integration_time_output'length)); 
			if(debounce > 0) then debounce <= debounce -1;end if;	
		end if;
	end if;
	end process;
	

end Behavioral;


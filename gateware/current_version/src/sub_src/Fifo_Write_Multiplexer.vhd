----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    09:13:06 02/20/2019 
-- Design Name: 
-- Module Name:    Fifo_Multiplexer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Fifo_Write_Multiplexer is
    Port ( 
				clk : in STD_LOGIC;
				selectWriteFIFO : in STD_LOGIC;
				writeReq : in  STD_LOGIC;
				full : out  STD_LOGIC;
				dataIn : in  STD_LOGIC_VECTOR (7 downto 0);
				writeReq_0 : out  STD_LOGIC;
				writeReq_1 : out  STD_LOGIC;
				full_0 : in  STD_LOGIC;
				full_1 : in  STD_LOGIC;
				dataIn_0 : out  STD_LOGIC_VECTOR (7 downto 0);
				dataIn_1 : out  STD_LOGIC_VECTOR (7 downto 0));
end Fifo_Write_Multiplexer;

architecture Behavioral of Fifo_Write_Multiplexer is

begin

	   Process (clk,selectWriteFIFO)
   Begin
 	  -- If clk'event and clk = '1' Then
			If selectWriteFIFO = '0' Then
				writeReq_0 	<= writeReq;
				writeReq_1 	<= '0';
				full 			<= full_0;
				dataIn_0 	<= dataIn;
			Else
				writeReq_1 	<= writeReq;
				writeReq_0 	<= '0';
				full 			<= full_1;
				dataIn_1 	<= dataIn;
			  End If;
		--end if;
	end process;
	
end Behavioral;


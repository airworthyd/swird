----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    14:39:21 09/17/2019 
-- Design Name: 
-- Module Name:    Setup_sensor_control_register - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Setup_sensor_control_register is
    Port ( rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           fsync : in  STD_LOGIC;
           data : out  STD_LOGIC);
end Setup_sensor_control_register;

architecture Behavioral of Setup_sensor_control_register is

signal fsync_delay_counter : integer range 0 to 7 :=0;
signal last_fsync : std_logic := '0';
signal bitCounter : integer range 0 to 31 := 0;
signal dataArray : std_logic_vector (31 downto 0) := x"aaaaeeee"; --- >  Data for register 
begin



 Process (clk)

	Begin 
	
	-- under nondisclosure
	-- dataArray<= ;
	
	
		IF rising_edge(clk)  THEN
			If RST = '0' Then
				if ((fsync = '1') and (last_fsync = '0')) then
					last_fsync <= '1';
					fsync_delay_counter <= 0;
					bitCounter <= 0;
				end if;	
				if ((fsync = '0') and (last_fsync = '1')) then
					--last_fsync <= '0'; -- now not only once at first fsync
				end if;	
					
					
					
					-- wait 6 clocks  see ISC0002 DS. page 81
					if ( fsync_delay_counter < 6 ) then
						fsync_delay_counter <= fsync_delay_counter + 1;
					end if;
					-- send data bit by bit
					if ((fsync_delay_counter = 6) and (bitCounter < 31))then
						data <= dataArray(bitCounter);
						bitCounter <= bitCounter +1;
					end if;
			end if;
				
			
		end if;
		
		
	end process;
		



end Behavioral;


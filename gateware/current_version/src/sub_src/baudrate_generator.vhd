----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    13:01:47 12/10/2019 
-- Design Name: 
-- Module Name:    baudrate_generator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity baudrate_generator is
    Port ( clk : in  STD_LOGIC;
           en_16_x_baud : out  STD_LOGIC);
end baudrate_generator;

architecture Behavioral of baudrate_generator is
signal baudCount : integer range 0 to 70 :=0;
begin
process(clk) -- setting the UART baudrate   baudCount = (clkFrequency / (16 * baudrate))-1;
						-- clkFrequency = 125 MHz
		begin	
			if rising_edge(clk) then
				if baudCount >= 67 then  -- <-- set baudCount here  //  67 = 115200
					baudCount <= 0;
					en_16_x_baud <= '1';
				else
					baudCount <= baudCount + 1;
					en_16_x_baud <= '0';
				end if;
			end if;
	end process;

end Behavioral;


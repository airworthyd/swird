----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter 
-- 
-- Create Date:    13:02:05 02/20/2019 
-- Design Name: 
-- Module Name:    AFE_ctrl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AFE_ctrl is
generic(
	PixelPerLine : integer := 640;
	NumberOfLines : integer := 512   
	);
    Port ( selectWriteFIFO : out  STD_LOGIC;
           selectReadFIFO : out  STD_LOGIC;
           clk : in  STD_LOGIC;
			  sample_clk : in std_logic;
           dataOut : out  STD_LOGIC_VECTOR (7 downto 0);
           fifo_wrReq : out  STD_LOGIC := '0';
           fifoWriteFull : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           dataIn : in  STD_LOGIC_VECTOR (7 downto 0);
			  startSendingUDP : out std_logic;
			  lsync : out std_logic;
			  testoutput : out std_logic;
			  lineIdx : in STD_LOGIC_VECTOR (9 downto 0); 	-- max 1024 lines
			  imageIdx : in STD_LOGIC_VECTOR (15 downto 0);  -- max 65535 Images
			  pixelIdx : out STD_LOGIC_VECTOR (9 downto 0));
end AFE_ctrl;

architecture Behavioral of AFE_ctrl is

-- under nondisclosur


end Behavioral;




----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    12:53:20 09/16/2019 
-- Design Name: 
-- Module Name:    Inst_servo_ctrl - Behavioral 
-- Project Name: 		FluoTrack
-- Target Devices: 	
-- Tool versions: 
-- Description: 
-- 	the input set value (in percent) is transfered in an 50Hz PWM signal to control a 
-- 	standard servo 
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- 
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Servo_ctrl is
    Port ( 	rst : in  STD_LOGIC;
				clk : in  STD_LOGIC;
				duty : in  STD_LOGIC_VECTOR(7 downto 0);				-- in percent
				servo1_sig : out  STD_LOGIC); 							-- PWM signal
end Servo_ctrl;

architecture Behavioral of Servo_ctrl is
	signal servo_dutycycle_signal : integer range 0 to 300 := 70;
	signal counter : integer range 0 to 2500000;
	constant divider : integer := 1000;   
	signal prescaler : integer range 0 to 1250 :=0;
begin
	Process(clk)
	begin
	IF (falling_edge(clk)) then 
		If rst = '0' Then
			if prescaler = 9 then
				servo_dutycycle_signal <= to_integer(unsigned(duty)) + 51; 
				prescaler <= 0;
				if counter = (divider-1) then
					counter <= 0;
					servo1_Sig <= '1';
				else
					counter <= counter + 1;
				end if;
				if (counter = servo_dutycycle_signal) or (counter = 150) then  
					servo1_Sig <= '0';
				end if;
			else
				prescaler <= prescaler +1;
			end if;	
		end if;
	end if;
	end process;
end Behavioral;



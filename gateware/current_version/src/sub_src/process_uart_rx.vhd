----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    14:49:13 12/10/2019 
-- Design Name: 
-- Module Name:    process_uart_rx - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity process_uart_rx is
    Port ( clk : in  STD_LOGIC;
           uart_data_in : in  STD_LOGIC_VECTOR (7 downto 0);
           uart_buffer_read : out  STD_LOGIC;
           uart_data_present : in  STD_LOGIC;
			  testOut : out std_logic;
			  integration_time_uart : out  STD_LOGIC_VECTOR (7 downto 0):=x"00";
			  aperture_uart : out STD_LOGIC_VECTOR (7 downto 0):=x"00";
			  pixel_threshold_uart : out std_logic_vector (7 downto 0) := x"0A"
			 );
end process_uart_rx;

architecture Behavioral of process_uart_rx is

constant header_aperture : std_logic_vector(7 downto 0) := x"61";
constant header_exposure : std_logic_vector(7 downto 0) := x"62";
constant header_pixel_threshold : std_logic_vector(7 downto 0) := x"63";

type state_type is (s0, s1, s2, s3);
signal state   : state_type := s0;
begin
Process(clk)
	begin
		IF (rising_edge(clk)) then 
			if(uart_data_present = '1')then
				case state is
					when s0 =>  -- idle
						if (uart_data_in = header_aperture) then state <= s1; end if;
						if (uart_data_in = header_exposure) then state <= s2; end if;	
						if (uart_data_in = header_pixel_threshold) then state <= s3; end if;	
					when s1 =>  -- Header for apertur received
						state <= s0;
						aperture_uart <= uart_data_in;
					when s2 =>  -- Header for exposure time received
						-- time quantum = 78us
						state <= s0;
						integration_time_uart <= uart_data_in;
					when s3 =>  -- Header for wihte pixel threshold	
						testOut<= '1';
						state <= s0;
						pixel_threshold_uart <= uart_data_in;
				end case;
				uart_buffer_read <= '1';
			else
				testOut<= '0';
				uart_buffer_read <= '0';
			end if;
			 
			
		end if;
	
	end process;

end Behavioral;


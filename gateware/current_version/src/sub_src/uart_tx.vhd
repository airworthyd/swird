----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    12:50:53 06/27/2019 
-- Design Name: 
-- Module Name:    uart_tx - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity uart_tx is
    Port ( send_ore : in  STD_LOGIC;
           data : in  STD_LOGIC_VECTOR (7 downto 0);
           serial_out : out  STD_LOGIC;
           clk : in  STD_LOGIC);
end uart_tx;

architecture Behavioral of uart_tx is
signal prescaler : integer range 0 to 125000000 := 0 ;
signal index : integer range 0 to 10 := 0;
signal old_send_ore : std_logic := '0';
signal sendingNow : std_logic :='0';
begin
	Process (clk)
	Begin
		IF rising_edge(clk)  THEN
			if ((send_ore = '1') and (old_send_ore = '0') and (sendingNow = '0')) then
				old_send_ore <= '1';
				index <= 0;
				prescaler <= 0;
				sendingNow <= '1';
			end if;
			if ((send_ore = '0') and (old_send_ore = '1') and (sendingNow = '0')) then
				old_send_ore <= '0';
			end if;
			prescaler <= prescaler +1;
			if (prescaler = 1085)  then 
				prescaler <= 0;
				if (index = 0)then serial_out <='0';  end if;  
				if (index = 1)then serial_out <= data(0); end if;
				if (index = 2)then serial_out <= data(1);  end if;
				if (index = 3)then serial_out <= data(2);  end if;
				if (index = 4)then serial_out <= data(3);  end if;
				if (index = 5)then serial_out <= data(4);  end if;
				if (index = 6)then serial_out <= data(5);  end if;
				if (index = 7)then serial_out <= data(6);  end if;
				if (index = 8)then serial_out <= data(7);  end if;
				if (index = 9)then serial_out <= '1'; sendingNow <= '0';  end if; 
				if ( sendingNow = '1') then index <= index +1; end if;
			end if;
		end if;
	end process;
end Behavioral;


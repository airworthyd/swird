----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    09:21:48 06/07/2019 
-- Design Name: 
-- Module Name:    sensor_ctrl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use IEEE.NUMERIC_STD.ALL;

entity sensor_ctrl is
generic(
	PixelPerLine : integer := 640;
	NumberOfLines : integer := 512
	);
    Port ( rst : in  STD_LOGIC;
			  afe_clk : in  STD_LOGIC;
           afe_lsync : in  STD_LOGIC;
           sensor_master_clk : out  STD_LOGIC;
           sensor_lsync : out  STD_LOGIC;
           sensor_fsync : out  STD_LOGIC;
			  sensor_field : out  STD_LOGIC;
			  sensor_power_enabled : in std_logic;
			  sensor_reset : out std_logic;
			  lineIdx : out STD_LOGIC_VECTOR (9 downto 0); 	
			  imageIdx : out STD_LOGIC_VECTOR (15 downto 0);
			  integration_time : in STD_LOGIC_VECTOR (8 downto 0));
end sensor_ctrl;

architecture Behavioral of sensor_ctrl is
signal prescaler: signed(7 downto 0);
signal afe_lsync_counter : integer range 0 to 700;
signal image_counter : integer range 0 to 65535 := 0;
signal line_counter : integer range 0 to 7000 := 0;
signal integration_counter : integer range 0 to 7000 := 0; 
signal old_afe_lsync : std_logic := '0';
signal sensor_field_signal : std_logic := '0';
signal sensor_fsync_signal : std_logic := '0';
begin

	Process (afe_clk)
	Begin
		IF rising_edge(afe_clk)  THEN
		
		If RST = '1' Then
				prescaler <= x"00";
				afe_lsync_counter <= 0;
				line_counter <= 0;
				integration_counter <= 0; 
				old_afe_lsync <= '0';
				sensor_reset <= '0';
				sensor_fsync_signal <= '0';
		else
		
		
			-- if sensor is powered, we can loosen the reset and start the clocks
		
				if sensor_power_enabled = '1' then
					sensor_reset <= '1';  -- reset is active low
					-- the afe clock is divided by 2, since the afe is sampling only on rising edges, while the sensor is shifting the pixel on both edges
					-- max. sensor_clk is 5MHz , i.e. 10M Pixel/s
					prescaler <= prescaler + 1;
					sensor_master_clk <= prescaler(1);  -- afe_clk division by 4
					
						-- catch rising edge of afe lsync
						if afe_lsync = '1' and old_afe_lsync = '0' then
							afe_lsync_counter <= 0;
							lineIdx <= std_logic_vector(to_unsigned(line_counter, lineIdx'length));
							if(line_counter = 1) then
								imageIdx <= std_logic_vector(to_unsigned(image_counter, imageIdx'length));
							end if;
							line_counter <= line_counter +1; 
							if integration_counter < 7000 then
								integration_counter <= integration_counter +1;
							end if;
						end if;
						old_afe_lsync <= afe_lsync;
						
						-- lsync
						if afe_lsync_counter < 100 then
							afe_lsync_counter <= afe_lsync_counter +1;
						end if;
						
						if afe_lsync_counter = 9 then    
							sensor_lsync <= '1';
						end if;
						
						if afe_lsync_counter = 19 then  
							sensor_lsync <= '0';
						end if;
					
						if ((line_counter = (NumberOfLines/2)+3) )then 
						
						end if;

						if ((line_counter = (NumberOfLines/2)+4) ) then
							sensor_field <= '0';
						end if;
						
						-- fsync
						if line_counter = (NumberOfLines)+5 then
							line_counter <= 0;
							sensor_fsync_signal <= '1';
							integration_counter <= 0;
							
							image_counter <= image_counter +1;
						else
							if ((integration_counter = 516 -  (to_integer(unsigned(integration_time)))) and (afe_lsync_counter =100)) then 
								sensor_fsync_signal <= '0';
							end if;
						end if;
						sensor_fsync <= sensor_fsync_signal;

				else -- if sensor is not powered
					sensor_reset <= '0';  -- reset is active low
					sensor_master_clk <= '0';
					sensor_lsync <= '0';
					sensor_fsync <= '0';
					sensor_fsync_signal <= '0';
					sensor_field <= '0';
					
				end if; -- if sensor power is enabled
			end if; -- if RST
		end if; -- if rising edge
	end process;

end Behavioral;


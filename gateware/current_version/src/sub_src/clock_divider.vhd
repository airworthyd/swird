----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    10:34:01 09/17/2019 
-- Design Name: 
-- Module Name:    clock_divider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                       
--                                                                           
------------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AFE_clockDivider is
    Port ( 		clk1 : in  STD_LOGIC;
				outputP : out  STD_LOGIC;
				outputN : out  STD_LOGIC);
end AFE_clockDivider;

architecture Behavioral of AFE_clockDivider is
signal prescaler: signed(7 downto 0);
signal prescaler2: signed(7 downto 0);
begin

 Process (clk1)
	Begin
		IF rising_edge(clk1)  THEN
				prescaler <= prescaler + 1;
				outputP <= prescaler(0);
				outputN <= not prescaler(0);
		end if;
	end process;
end Behavioral;




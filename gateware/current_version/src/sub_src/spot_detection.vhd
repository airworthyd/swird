----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    13:55:48 10/23/2019 
-- Design Name: 
-- Module Name:    spot_detection - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--		this module is analyzing every incoming pixel
--		if the pixelvalue is below a threshold, and it is not a fix/static = hot pixel, its coordinates (lineIdx and pixelIdx) are stored 
--		in a BRAM
--		at the end of an image, the centre of the white dot are stored in spotX and spotY
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- 	todo: calc centre of white dot
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                  
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity spot_detection is
generic(
	PixelPerLine : integer := 640;
	NumberOfLines : integer := 512   
	);
    Port ( 
		rst : in std_logic;
		clk : IN std_logic;          
		send_ore : OUT std_logic;
		data_out : OUT std_logic_vector(7 downto 0);
		pixelIdx : in std_logic_vector (9 downto 0);
		lineIdx : in std_logic_vector (9 downto 0);
		pixelValue : in std_logic_vector (7 downto 0);
		hot_wea : out STD_LOGIC_VECTOR(0 DOWNTO 0);
		hot_addra : out STD_LOGIC_VECTOR(9 DOWNTO 0);
		hot_douta : in STD_LOGIC_VECTOR(31 DOWNTO 0);
		white_wea : out STD_LOGIC_VECTOR(0 DOWNTO 0);
		white_addrIn : out STD_LOGIC_VECTOR(7 DOWNTO 0);
		white_din : out STD_LOGIC_VECTOR(31 DOWNTO 0);
		white_addrOut : out STD_LOGIC_VECTOR(7 DOWNTO 0);
		white_dout : in STD_LOGIC_VECTOR(31 DOWNTO 0);
		spotX : out std_logic_vector (9 downto 0);
		spotY : out std_logic_vector (9 downto 0);
		white_pixel_threshold : in std_logic_vector (7 downto 0):= x"0A");
end spot_detection;

architecture Behavioral of spot_detection is
	signal prescaler : integer range 0 to 125000000 := 0 ;
	signal index : integer range 0 to 255 := 0;
	signal index2 : integer range 0 to 255 := 0;
	signal index3 : integer range 0 to 1024 := 0;
	signal x_max : integer range 0 to 640 := 123;
	signal y_max : integer range 0 to 512 := 321;
	signal tmp : std_logic_vector (15 downto 0);
	signal oldPixelIdx : std_logic_vector (9 downto 0) := b"0000000000";
	signal resetIndex : std_logic := '0';
	signal lineIdx_signal : std_logic_vector (9 downto 0):= b"0000000000";
	signal pixelIdx_signal : std_logic_vector (9 downto 0):= b"0000000000";
	signal hot_lineIdx_signal : std_logic_vector (9 downto 0):= b"0000000000";
	signal hot_pixelIdx_signal : std_logic_vector (9 downto 0):= b"0000000000";
	signal state : integer range 0 to 10 := 1;
	signal pixelValue_signal : std_logic_vector (7 downto 0):= b"00000000";
	signal finishedImage_signal : std_logic := '0';
	signal xDotMax_signal : integer range 0 to 1000 := 0;
	signal xDotMin_signal : integer range 0 to 1000 := 700;
	signal yDotMax_signal : integer range 0 to 1000 := 0;
	signal yDotMin_signal : integer range 0 to 1000 := 700;
	signal xDotCentre_signal : integer range 0 to 1000 := 0;
	signal yDotCentre_signal : integer range 0 to 1000 := 0;
begin
Process (clk)
	Begin
		IF falling_edge(clk)  THEN
			If rst = '0' Then
				lineIdx_signal <= lineIdx;
				-- bright spot detection
				hot_wea <= b"0";
				white_wea <= b"0";	
				-- update hot pixel BRAM address
				if(state = 1) then
					hot_addra <= std_logic_vector(to_unsigned(index3, hot_addra'length));
					state <=2;		
				-- if new image started: init parameters
				-- else if within the image: store pixelvalue, pixelIdx and read hotpixel from BRAM	
				elsif(state=2)then
					if ((index < 198) and finishedImage_signal = '0'  and (oldPixelIdx /= pixelIdx)) then
						oldPixelIdx <= pixelIdx;
						if((to_integer(unsigned(lineIdx)) < 30)) then
							index3 <= 0;
							xDotMax_signal <= 0;
							xDotMin_signal <= PixelPerLine; --700;
							yDotMax_signal <= 0;
							yDotMin_signal <= NumberOfLines; --700;
						end if;											
						if((to_integer(unsigned(lineIdx)) >= 20) and (to_integer(unsigned(lineIdx)) < NumberOfLines-1) and (to_integer(unsigned(pixelIdx)) < (PixelPerLine-25))  and (to_integer(unsigned(pixelIdx)) > 5 )  )then
							-- lese hot pixel addr n
							hot_lineIdx_signal <= hot_douta( 9 downto  0);
							hot_pixelIdx_signal <= hot_douta(25 downto 16);
							pixelValue_signal <= pixelValue;
							pixelIdx_signal <= pixelIdx;
							state <= 3;
						end if;
						if((to_integer(unsigned(lineIdx)) = NumberOfLines-1))then
							finishedImage_signal <= '1';
						end if;
					end if;		
					-- wenn schon 198 weie pixel gefunden wurden, wird die analyse abgebrochen und die daten werden ausgegeben
					if(index >=198 or finishedImage_signal = '1')then
						state <= 4;
					end if;
				-- analyze pixelValue: if it is white and it is not a hot pixel, safe it in the white BRAM	
				elsif(state = 3) then
					--if pixel line and pixel index == a hot pixel, skip it
					if ( to_integer(unsigned(hot_lineIdx_signal))  <= to_integer(unsigned(lineIdx))) and
   					 ( to_integer(unsigned(hot_pixelIdx_signal)) <= (to_integer(unsigned(pixelIdx_signal))) )then -- or
						 --( to_integer(unsigned(hot_lineIdx_signal))  < to_integer(unsigned(lineIdx)))  then	
						
						index3 <= index3 +1;
					else		
					--else put it in the white buffer and see, if its max or min
						if(to_integer(unsigned(pixelValue_signal)) < to_integer(unsigned(white_pixel_threshold))) then   -- black pixel = ff; white pixel = 0
							white_din(25 downto 16) <=  pixelIdx_signal;
							white_din( 9 downto  0) <=  lineIdx;	
							-- save maxima and minima of colomn and row
							
							--if( to_integer(unsigned(pixelIdx_signal)) < xDotMin_signal)then xDotMin_signal <= to_integer(unsigned(pixelIdx_signal)); end if;
							--if( to_integer(unsigned(lineIdx)) > yDotMax_signal)then 
								yDotMax_signal <= to_integer(unsigned(lineIdx));
								xDotMax_signal <= to_integer(unsigned(pixelIdx_signal));
							--end if;
							--if( to_integer(unsigned(pixelIdx_signal)) > xDotMax_signal)then xDotMax_signal <= to_integer(unsigned(pixelIdx_signal)); end if;
							--if( to_integer(unsigned(lineIdx)) < yDotMin_signal)then yDotMin_signal <= to_integer(unsigned(lineIdx)); end if;
							white_addrIn <= std_logic_vector(to_unsigned(index, white_addrIn'length));
							white_wea <= b"1";
							index <= index +1; 
						end if; -- pixelValue < threshold
					end if;
					state <= 1;	
				elsif(state = 4) then	
					
					
					
					state <= 5; --standard function
					--state <= 6; -- for debug: now all white pixel are send via uart
					-- todo: calc centre of white dot
					xDotCentre_signal <= xDotMax_signal; --(xDotMax_signal - xDotMin_signal)/2 + xDotMin_signal;
					yDotCentre_signal <= yDotMax_signal; --(yDotMax_signal - yDotMin_signal)/2 + yDotMin_signal;
					
					spotX <= std_logic_vector(to_unsigned(xDotMax_signal, 10));
					spotY <= std_logic_vector(to_unsigned(yDotMax_signal, 10));
					
					xDotMax_signal <= 0;
					yDotMax_signal <= 0;
					
					
--					if((to_integer(unsigned(lineIdx_signal)) = 0) and (to_integer(unsigned(pixelIdx)) = 0)) then
--					--		resetIndex <= '0';
--							index <= 0;
--							state <= 1;
--							finishedImage_signal <= '0';
--					end if;
					
					
					
				-- send coordinates of white dot via uart	
				elsif(state = 5) then
					
					prescaler <= prescaler +1;
--					if prescaler = 5300000  then
--						--if(xDotMax_signal /= 0)then
--						--	spotX <= std_logic_vector(to_unsigned(xDotCentre_signal, 10));
--						--end if;
--						--if(yDotMax_signal /= 0)then
--						--	spotY <= std_logic_vector(to_unsigned(yDotCentre_signal, 10));
--						--end if;
--						send_ore <= '1';
--						data_out <= std_logic_vector(to_unsigned(2, data_out'length)(7 downto 0));
--					elsif prescaler = 5400000  then
--						send_ore <= '1';
--						data_out <= std_logic_vector(to_unsigned(3, data_out'length)(7 downto 0));
--					elsif prescaler = 5500000  then
--						send_ore <= '1';
--						data_out <= std_logic_vector(to_unsigned(index3, data_out'length)(7 downto 0));
--					elsif prescaler = 5600000  then
--						send_ore <= '1';
--						data_out <= std_logic_vector(to_unsigned(0, data_out'length)(7 downto 0));	
--					elsif prescaler = 5700000  then
--						send_ore <= '1';
--						--data_out <= std_logic_vector(to_unsigned(xDotMax_signal, 16)(15 downto 8));
--						data_out <= std_logic_vector(to_unsigned(xDotCentre_signal, 16)(15 downto 8));
--					elsif prescaler = 5800000  then
--						send_ore <= '1';
--						--data_out <= std_logic_vector(to_unsigned(xDotMax_signal, 16)(7 downto 0));		
--						data_out <= std_logic_vector(to_unsigned(xDotCentre_signal, 16)(7 downto 0));							
--					elsif prescaler = 5900000  then
--						send_ore <= '1';
--						--data_out <= std_logic_vector(to_unsigned(yDotMax_signal, 16)(15 downto 8));
--						data_out <= std_logic_vector(to_unsigned(yDotCentre_signal, 16)(15 downto 8));
--					elsif prescaler = 6000000  then
--						send_ore <= '1';
--						--data_out <= std_logic_vector(to_unsigned(yDotMax_signal, 16)(7 downto 0));
--						data_out <= std_logic_vector(to_unsigned(yDotCentre_signal, 16)(7 downto 0));		
--					elsif prescaler = 6100000  then
					if prescaler = 61  then		
						prescaler <= 0;
						resetIndex <= '1';
					else
						send_ore <= '0';
					end if;
					if(resetIndex = '1') then
						if((to_integer(unsigned(lineIdx_signal)) = 0) and (to_integer(unsigned(pixelIdx)) = 0)) then
							resetIndex <= '0';
							index <= 0;
							index3 <= 0;
							state <= 1;
							finishedImage_signal <= '0';
						end if;
					end if;		
					
					
					
				-- debug only: send all pixel in white BRAM				
				elsif(state = 6) then	
				-- send via uart		
					prescaler <= prescaler +1;
					if prescaler =    5300000  then
						send_ore <= '1';
						data_out <= std_logic_vector(to_unsigned(2, tmp'length)(7 downto 0));
					elsif prescaler = 5400000  then
						send_ore <= '1';
						data_out <= std_logic_vector(to_unsigned(3, tmp'length)(7 downto 0));
					elsif prescaler = 5500000  then
						send_ore <= '1';
						data_out <= std_logic_vector(to_unsigned(0, tmp'length)(7 downto 0));
					elsif prescaler = 5600000  then
						send_ore <= '1';
						data_out <= std_logic_vector(to_unsigned(index2, tmp'length)(7 downto 0));
						white_addrOut <= std_logic_vector(to_unsigned(index2, white_addrOut'length));
					elsif prescaler = 5700000  then
						send_ore <= '1';
						white_addrOut <= std_logic_vector(to_unsigned(index2, white_addrOut'length));
						data_out <= white_dout(31 downto 24);
					elsif prescaler = 5800000  then
						send_ore <= '1';
						white_addrOut <= std_logic_vector(to_unsigned(index2, white_addrOut'length));
						data_out <= white_dout(23 downto 16);
					elsif prescaler = 5900000  then
						send_ore <= '1';
						white_addrOut <= std_logic_vector(to_unsigned(index2, white_addrOut'length));
						data_out <= white_dout(15 downto 8);
					elsif prescaler = 6000000  then
						send_ore <= '1';
						white_addrOut <= std_logic_vector(to_unsigned(index2, white_addrOut'length));
						data_out <= white_dout(7 downto 0);
					elsif prescaler = 6300000  then	
						if ( index2 < index )then 
							index2 <= index2 +1;
						else 
							index2 <= 0; 
							resetIndex <= '1';
						end if;
						prescaler <= 0;
					else
						send_ore <= '0';
					end if;	
					if(resetIndex = '1') then
						if((to_integer(unsigned(lineIdx_signal)) = 0) and (to_integer(unsigned(pixelIdx)) = 0)) then
							resetIndex <= '0';
							index <= 0;
							state <= 1;
							finishedImage_signal <= '0';
						end if;
					end if;	
				end if; -- state
			end if; -- reset
		end if; -- falling edge clk
	end process;

end Behavioral;


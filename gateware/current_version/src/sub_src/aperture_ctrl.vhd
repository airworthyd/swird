----------------------------------------------------------------------------------
-- Company: Uni Würzburg Info 8     
-- Engineer: Thomas Walter
-- 
-- Create Date:    15:18:21 11/12/2019 
-- Design Name: 
-- Module Name:    aperture_ctrl - Behavioral 
-- Project Name: 		FluoTrack 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--		generating servo PWM signal, that can be changed by buttoninterface 
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                          
--    Copyright (C) 2019 Authors                                               
--                                                                          
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
                                    
------------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity aperture_ctrl is
 Port ( 	rst : in  STD_LOGIC;
			clk : in  STD_LOGIC;
			up : in  STD_LOGIC;
			down : in  STD_LOGIC;
			aperture_uart : in std_logic_vector(7 downto 0):=x"00";
			servo1_sig : out  STD_LOGIC;
			servo_index : out std_logic_vector(7 downto 0));
end aperture_ctrl;

architecture Behavioral of aperture_ctrl is
	signal servo_dutycycle_signal : integer range -100 to 100 := 0;
	signal servo_dutycycle_step_signal : integer range 0 to 5 := 2;
	signal last_up : std_logic := '0';
	signal last_down : std_logic := '0';
	signal counter : integer range 0 to 2500000;
	constant divider : integer := 1000;  
	signal debounce : integer range 0 to 50 := 0;
	signal prescaler : integer range 0 to 1250 :=0;
	
	signal aperture_uart_signal : integer range -100 to 100 := 0;
	signal servo_dutycycle_signal_upper_limit : integer range -100 to 100 := 0;
	signal servo_dutycycle_signal_lower_limit : integer range -100 to 100 := 0;
	
	
begin
	Process(clk)
	begin
	IF (falling_edge(clk)) then 
		If rst = '0' Then
			if prescaler = 9 then
				prescaler <= 0;
				IF ((up = '1') and (last_up = '0') and (down = '0') and (debounce = 0) and (servo_dutycycle_signal < (100 - servo_dutycycle_step_signal)))  THEN -- max. 2ms
					last_up <= '1';
					servo_dutycycle_signal <= servo_dutycycle_signal + servo_dutycycle_step_signal; -- stepwidth
					debounce <= 50;
				end if;
				IF ((down = '1') and (last_down = '0') and (up = '0') and (debounce = 0) and (servo_dutycycle_signal > (-50 + servo_dutycycle_step_signal)))  THEN -- min. 1ms
					last_down <= '1';
					servo_dutycycle_signal <= servo_dutycycle_signal - servo_dutycycle_step_signal; -- stepwidth
					debounce <= 50;
				end if;
				IF ((up = '0') and (last_up = '1') and (debounce = 0) )  THEN
					last_up <= '0';
					debounce <= 50;
				end if;
				IF ((down = '0') and (last_down = '1') and (debounce = 0) )  THEN
					last_down <= '0';
					debounce <= 50;
				end if;	
				-- debounce of buttons
				if(debounce > 0) then debounce <= debounce -1;end if;	
				-- set output to '1'
				if counter = (divider-1) then
					counter <= 0;
					servo1_Sig <= '1';
				else
					counter <= counter + 1;
				end if;

				--  combine uart input with button input
				if(to_integer(signed(aperture_uart)) > 100) then 
					aperture_uart_signal <= 100;
				elsif(to_integer(signed(aperture_uart)) < 50)then
					aperture_uart_signal <= 50;
				else
					aperture_uart_signal <= to_integer(signed(aperture_uart));
				end if;
					
				servo_dutycycle_signal_upper_limit <= 100 - aperture_uart_signal;
				servo_dutycycle_signal_lower_limit <= 50 - aperture_uart_signal;
				
				if(servo_dutycycle_signal >  servo_dutycycle_signal_upper_limit) then
					servo_dutycycle_signal <= servo_dutycycle_signal_upper_limit;
				end if;
				if(servo_dutycycle_signal <  servo_dutycycle_signal_lower_limit)then
					servo_dutycycle_signal <= servo_dutycycle_signal_lower_limit;
				end if;

				-- set output to '0'
				if (counter = (servo_dutycycle_signal + aperture_uart_signal)) then
					servo1_Sig <= '0';
				end if;
				-- index that can be sent over ethernet and represented in one byte
				servo_index <= std_logic_vector(to_unsigned(((servo_dutycycle_signal + aperture_uart_signal) - 50), servo_index'length));
			else
				prescaler <= prescaler +1;
			end if;
		end if;
	end if;
	end process;
end Behavioral;


-------------------------------------------------------------------------------
-- Project: FlouTrack
-- Author(s): Thomas Walter
-- E-Mail:  thomas.walter@uni-wuerzburg.de
-- Created: 2019   
-------------------------------------------------------------------------------
--    This file is part of FlouTrack.
--
--    FlouTrack is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    FlouTrack is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License.
--    If not, see <http://www.gnu.org/licenses/>.
    
    

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg_rst_gen.all;
use work.pkg_pkt_trigger.all;

library UNISIM;
use UNISIM.Vcomponents.ALL; 

Entity top Is
    Port (
     -- CLock form on board oscilator
     sys_clk   : In  std_logic;
     
     -- PHY control - reset
     rstn      : Out std_logic;
     
     -- PHY Tx side
     tx_clk	   : Out std_logic;
     tx_en		: Out std_logic;
     tx_er		: Out std_logic;
     tx_data	: Out std_logic_vector(7 downto 0);

     -- PHY Rx side
     rx_clk		: In  std_logic;
     rx_dv		: In  std_logic;
     rx_er		: In  std_logic;
     rx_data   : In  std_logic_vector(7 downto 0);

	  -- select fifo
	  selectReadFIFO	: In std_logic;
	  selectWriteFIFO	: In std_logic;
	  
	  -- AFE Interface
	  
	  -- SDI from C to AFE
		sclk_uC : in  STD_LOGIC;
		sen_uC : in  STD_LOGIC;
		sdi_uC : in  STD_LOGIC;
		sdo_uC : out  STD_LOGIC;
		sclk_AFE : out  STD_LOGIC;
		sen_AFE : out  STD_LOGIC;
		sdi_AFE : out  STD_LOGIC;
		sdo_AFE : in  STD_LOGIC;
	  
		AFE_XSH2 : in  STD_LOGIC;
		AFE_XSH3 : in  STD_LOGIC;
		AFE_XSH4 : in  STD_LOGIC;
		AFE_GPIO0 : in  STD_LOGIC;
		AFE_GPIO2 : in  STD_LOGIC;
		RCLKN : out  STD_LOGIC;
		RCLKP : inout std_logic;
		
		AFE_Data : in STD_LOGIC_VECTOR (7 downto 0);

		AFE_LSYNC : out  STD_LOGIC;
		AFE_XST : in  STD_LOGIC;
		AFE_XCLR : in  STD_LOGIC;
		AFE_XP1 : in  STD_LOGIC;
		AFE_XP2 : in  STD_LOGIC;
		AFE_XP3 : in  STD_LOGIC;

		AFE_XRS : in  STD_LOGIC;
		AFE_XCP : in  STD_LOGIC;
		AFE_X2L : in  STD_LOGIC;
		AFE_X1L : in  STD_LOGIC;
		AFE_XP4 : in  STD_LOGIC;
	  
	  -- Sensor interface
	   S_FSYNC : out  STD_LOGIC;
	   S_LSYNC : out  STD_LOGIC;
		S_RST : out  STD_LOGIC;
		S_CLK : out  STD_LOGIC;
		S_FIELD : out  STD_LOGIC;
		S_DATA  : out  STD_LOGIC;
		
		LS_ENABLE : out  STD_LOGIC;
		
		uC_sensor_power_enable_indicator : in std_logic;
		sensor_can_be_powered : out std_logic;
		
	  -- Test Pad Interface
		tp2 : inout  STD_LOGIC;
		tp12 : out  STD_LOGIC;
		tp13 : out  STD_LOGIC;
		tp14 : out  STD_LOGIC;
		tp15 : out  STD_LOGIC;
		
		-- Button Interface from C
		uC_button_upLeft : in  STD_LOGIC;
		uC_button_upRight : in  STD_LOGIC;
		uC_button_downLeft : in  STD_LOGIC;
		uC_button_downRight : in  STD_LOGIC;
		
		-- Servo Interface
		servo_1 : out STD_LOGIC;  	-- Blende
		servo_2 : out STD_LOGIC;  	-- Serial Out
		servo_3 : out STD_LOGIC;	-- X_Servo
		servo_4 : out STD_LOGIC;	-- Y_Servo	
		
		-- USART interface between C and FPGA
		uC_UART_Tx : in  STD_LOGIC;
		uC_UART_Rx : out STD_LOGIC
		
   );
End top;

Architecture arch Of top Is 

	COMPONENT AFE_clockDivider
	PORT(
		clk1 : IN std_logic;    	
		outputP : OUT std_logic;
		outputN : OUT std_logic
		);
	END COMPONENT;

   Component ethtxclkio Is
	PORT
	(
		datain_h		: IN STD_LOGIC ;
		datain_l		: IN STD_LOGIC ;
		outclock		: IN STD_LOGIC ;
		dataout		: OUT STD_LOGIC 
	);
	End Component ethtxclkio;

	Component ethoutput
   GENERIC(
      bit_num : positive
           );
	PORT
	(
		datain_h		: IN STD_LOGIC_VECTOR (bit_num - 1 DOWNTO 0);
		datain_l		: IN STD_LOGIC_VECTOR (bit_num - 1 DOWNTO 0);
		outclock		: IN STD_LOGIC ;
		dataout		: OUT STD_LOGIC_VECTOR (bit_num - 1 DOWNTO 0)
	);
	End Component;
   
   Component image_line_fifo is
	   port (
   	wr_clk: IN std_logic;
	   rd_clk: IN std_logic;
   	din: IN std_logic_VECTOR(7 downto 0);
	   wr_en: IN std_logic;
   	rd_en: IN std_logic;
	   dout: OUT std_logic_VECTOR(7 downto 0);
   	full: OUT std_logic;
	   empty: OUT std_logic;
   	rd_data_count: OUT std_logic_VECTOR(9 downto 0));
   End Component;   
		
   Component udp_pkt_gen Is
	Generic(
      IGAP_LEN          : integer := 12;     -- 12 is minimul allowed by standard	
      DATA_LEN          : integer := 645; --1280;   -- 160 Events @ 64bits/Event
      HEADER_LEN        : integer := 66;     -- 8B Preambule + 14B Eth + 20B IP + 8B UDP + 16B OurHeader = 66B 
      LENGTH_OFF        : integer := 53;       -- Where to place length of data in bytes
      BOARD_ID_MAC_OFF  : integer := 17;       -- Where to place board ID into MAC address
      PACKET_ID_OFF     : integer := 55;       -- Where to place number of packet
      USR_FLG_OFF       : integer := 61        -- Where to place user flags
	);
	Port(
		CLK				: In Std_logic;
		RST				: In Std_logic;
		
		-- Control inouts
		START			: In  Std_logic; -- Starts sending packets
      READY       : out Std_logic; -- Indicates readiness to accept start
		USR_FLAGS	: In  Std_logic_vector(15 downto 0); -- added to header on positive edge of START
		SPOT_POS		: In  Std_logic_vector(31 downto 0); -- position of spot
		BOARD_ID    : In Std_logic_vector( 7 downto 0);
		LENGTH      : In Std_logic_vector(10 downto 0);
		
		-- Conection to packet data FIFO
		FIFO_RDREQ		: Out Std_logic;
		FIFO_RDEMPTY	: In  Std_logic;
		FIFO_DATA		: In  Std_logic_vector(7 downto 0);
		
		-- Connection to ethernet PHY
		PHY_EN 		: Out Std_logic;
		PHY_ER	 	: Out Std_logic;
		PHY_TX   	: Out Std_logic_vector(7 downto 0);
      PHY_READY   : in  Std_logic -- If 1 PHY is ready to capture data on rising edge
	);
	End Component udp_pkt_gen;
	
------------------------------------------------------------------------------
-- "Output    Output      Phase     Duty      Pk-to-Pk        Phase"
-- "Clock    Freq (MHz) (degrees) Cycle (%) Jitter (ps)  Error (ps)"
------------------------------------------------------------------------------
-- CLK_OUT1___125.000______0.000______50.0______161.194____212.907
-- CLK_OUT2___100.000______0.000______50.0______168.350____212.907
-- CLK_OUT3____31.250______0.000______50.0______213.092____212.907
-- CLK_OUT4____16.129____-37.742______50.0______243.273____212.907
--
------------------------------------------------------------------------------
-- "Input Clock   Freq (MHz)    Input Jitter (UI)"
------------------------------------------------------------------------------
-- __primary_________125.000____________0.010


-- The following code must appear in the VHDL architecture header:
------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG
component Clk_Generator
port
 (-- Clock in ports
  CLK_IN1           : in     std_logic;
  -- Clock out ports
  CLK_OUT1          : out    std_logic;
  CLK_OUT2          : out    std_logic;
  CLK_OUT3          : out    std_logic;
  CLK_OUT4          : out    std_logic;
  -- Status and control signals
  RESET             : in     std_logic;
  LOCKED            : out    std_logic
 );
end component;

	COMPONENT Fifo_Read_Multiplexer
	PORT(
		clk : IN std_logic;
		selectReadFIFO : IN std_logic;
		readReq : IN std_logic;
		dataOut_0 : IN std_logic_vector(7 downto 0);
		dataOut_1 : IN std_logic_vector(7 downto 0);
		empty_0 : IN std_logic;
		empty_1 : IN std_logic;
		rdUsedW_0 : IN std_logic_vector(9 downto 0);
		rdUsedW_1 : IN std_logic_vector(9 downto 0);          
		dataOut : OUT std_logic_vector(7 downto 0);
		empty : OUT std_logic;
		rdUsedW : OUT std_logic_vector(9 downto 0);
		readReq_0 : OUT std_logic;
		readReq_1 : OUT std_logic
		);
	END COMPONENT;

	COMPONENT Fifo_Write_Multiplexer
	PORT(
		clk : IN std_logic;
		selectWriteFIFO : IN std_logic;
		writeReq : IN std_logic;
		dataIn : IN std_logic_vector(7 downto 0);
		full_0 : IN std_logic;
		full_1 : IN std_logic;
		full : OUT std_logic;
		writeReq_0 : OUT std_logic;
		writeReq_1 : OUT std_logic;
		dataIn_0 : OUT std_logic_vector(7 downto 0);
		dataIn_1 : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;

	COMPONENT AFE_ctrl is
	generic(
     PixelPerLine : integer := 645;
	  NumberOfLines : integer := 512
	);
	PORT(
		clk : IN std_logic;
		sample_clk : in std_logic;
		fifoWriteFull : IN std_logic;
		rst : IN std_logic;
		dataIn : IN std_logic_vector(7 downto 0);          
		selectWriteFIFO : OUT std_logic;
		selectReadFIFO : OUT std_logic;
		dataOut : OUT std_logic_vector(7 downto 0);
		fifo_wrReq : OUT std_logic;
		startSendingUDP : OUT std_logic;
		lsync : OUT std_logic;
		testoutput : out std_logic;
		lineIdx : in STD_LOGIC_VECTOR (9 downto 0); 	
		imageIdx : in STD_LOGIC_VECTOR (15 downto 0);
		pixelIdx : out STD_LOGIC_VECTOR (9 downto 0)
		);
	END COMPONENT;

	COMPONENT sensor_ctrl
	generic(
     PixelPerLine : integer := 645;
	  NumberOfLines : integer := 512
	);
	PORT(
		rst : in  STD_LOGIC;
		afe_clk : IN std_logic;
		afe_lsync : IN std_logic;          
		sensor_master_clk : OUT std_logic;
		sensor_lsync : OUT std_logic;
		sensor_fsync : OUT std_logic;
		sensor_field : out  STD_LOGIC;
		sensor_power_enabled : in std_logic;
		sensor_reset : out std_logic;
		lineIdx : out STD_LOGIC_VECTOR (9 downto 0); 	
		imageIdx : out STD_LOGIC_VECTOR (15 downto 0);
		integration_time : in STD_LOGIC_VECTOR (8 downto 0)
		);
	END COMPONENT;

	COMPONENT Integration_time_ctrl
	PORT(
		rst : in  STD_LOGIC;
		clk : IN std_logic;
		up : IN std_logic;
		down : IN std_logic;          
		integration_time_input : in  STD_LOGIC_VECTOR (7 downto 0):= x"00";
		integration_time_output : out  STD_LOGIC_VECTOR (8 downto 0)
		);
	END COMPONENT;

	COMPONENT Servo_ctrl
	PORT(
		rst : IN std_logic;
		clk : IN std_logic;
		duty : IN std_logic_vector(7 downto 0);          
		servo1_Sig : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT aperture_ctrl
	PORT(		
		rst : in  STD_LOGIC;
		clk : in  STD_LOGIC;
		up : in  STD_LOGIC;
		down : in  STD_LOGIC;
		aperture_uart : in std_logic_vector(7 downto 0);
		servo1_sig : out  STD_LOGIC;
		servo_index : out std_logic_vector(7 downto 0)       
		);
	END COMPONENT;

	COMPONENT Setup_sensor_control_register
	PORT(
		rst : IN std_logic;
		clk : IN std_logic;
		fsync : IN std_logic;          
		data : OUT std_logic
		);
	END COMPONENT;

	COMPONENT uart_tx
	PORT(
		send_ore : IN std_logic;
		data : IN std_logic_vector(7 downto 0);
		clk : IN std_logic;          
		serial_out : OUT std_logic
		);
	END COMPONENT;

	COMPONENT spot_detection
	PORT(
		rst : IN std_logic;
		clk : IN std_logic;         
		send_ore : OUT std_logic;
		data_out : OUT std_logic_vector(7 downto 0);
		pixelIdx : in std_logic_vector (9 downto 0);
		lineIdx : in std_logic_vector (9 downto 0);
		pixelValue : in std_logic_vector (7 downto 0);
		hot_wea : out STD_LOGIC_VECTOR(0 DOWNTO 0);
		hot_addra : out STD_LOGIC_VECTOR(9 DOWNTO 0);
		hot_douta : in STD_LOGIC_VECTOR(31 DOWNTO 0);
		white_wea : out STD_LOGIC_VECTOR(0 DOWNTO 0);
		white_addrIn : out STD_LOGIC_VECTOR(7 DOWNTO 0);
		white_din : out STD_LOGIC_VECTOR(31 DOWNTO 0);
		white_addrOut : out STD_LOGIC_VECTOR(7 DOWNTO 0);
		white_dout : in STD_LOGIC_VECTOR(31 DOWNTO 0);
		spotX : out std_logic_vector (9 downto 0);
		spotY : out std_logic_vector (9 downto 0);
		white_pixel_threshold : in std_logic_vector (7 downto 0)
		);
	END COMPONENT;

	COMPONENT HotPixelRAM
	  PORT (
		 clka : IN STD_LOGIC;
		 ena : IN STD_LOGIC;
		 wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		 addra : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		 douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
	  );
	END COMPONENT;
	
	COMPONENT BRAM_dualPort
	  PORT (
		 clka : IN STD_LOGIC;
		 wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		 addra : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		 clkb : IN STD_LOGIC;
		 addrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
	  );
	END COMPONENT;

	COMPONENT uart_rx
	PORT(
		serial_in : IN std_logic;
		en_16_x_baud : IN std_logic;
		buffer_read : IN std_logic;
		buffer_reset : IN std_logic;
		clk : IN std_logic;          
		data_out : OUT std_logic_vector(7 downto 0);
		buffer_data_present : OUT std_logic;
		buffer_half_full : OUT std_logic;
		buffer_full : OUT std_logic
		);
	END COMPONENT;

	COMPONENT baudrate_generator
	PORT(
		clk : IN std_logic;          
		en_16_x_baud : OUT std_logic
		);
	END COMPONENT;

	COMPONENT process_uart_rx
	PORT(
		clk : IN std_logic;
		uart_data_in : IN std_logic_vector(7 downto 0);
		uart_data_present : IN std_logic;          
		uart_buffer_read : OUT std_logic;
		testOut : out std_logic;
		integration_time_uart : out  STD_LOGIC_VECTOR (7 downto 0);
		aperture_uart : out STD_LOGIC_VECTOR (7 downto 0);
		pixel_threshold_uart : out std_logic_vector (7 downto 0)
		);
	END COMPONENT;

	COMPONENT slow_clock_gen
	PORT(
		clk : IN std_logic; 
		RST : in std_logic;		
		slow_clk : OUT std_logic
		);
	END COMPONENT;

   -- Internal clock signal -- may be from pll
  	signal int_clk     : std_logic;
	signal pll_locked  : std_logic;

   -- Blinking of alive LEDs - one on system clock one on tx clock
	signal cnt: std_logic_vector(31 downto 0);
	signal cnt_tx: std_logic_vector(31 downto 0);


   -- Reset signals
   signal rst         : std_logic;
   signal rst1        : std_logic;
	signal rst_tx      : std_logic;
   signal rst_tx_d    : std_logic;
   signal rst_tx_dd   : std_logic;
   signal ext_rst     : std_logic;

   -- Clock to PHY
	signal mii_clk_tx  : std_logic;
   signal nmii_clk_tx : std_logic;
	
	-- Clock to AFE_ctrl
	signal afe_ctrl_clk : std_logic;
	
   -- Internal signals
	signal tx_en_i     : std_logic;
   signal tx_en_ii    : std_logic;
	signal tx_er_i     : std_logic;
	signal tx_data_i   : std_logic_vector(7 downto 0) := (others => '0');
   signal tx_data_ii  : std_logic_vector(7 downto 0)  := (others => '0');
   signal tx_ready_i  : std_logic;
	
	signal outs_i	    :std_logic_vector(9 downto 0);
	signal outs_o	    :std_logic_vector(9 downto 0);
	
	signal fifo_rdreq  : std_logic;
   signal fifo_data_in: std_logic_vector(7 downto 0);
   signal fifo_rdempty: std_logic;
   signal fifo_data   : std_logic_vector(7 downto 0);
   signal fifo_wrreq  : std_logic;

   -- Event fifo signals
   signal event_fifo_data_in  : std_logic_vector(7 downto 0);  --47
   signal event_fifo_data_out : std_logic_vector(7 downto 0);  --47
   signal event_fifo_wrreq    : std_logic;
   signal event_fifo_wrfull   : std_logic;
   signal event_fifo_rdreq    : std_logic;
   signal event_fifo_rdempty  : std_logic;
   signal event_fifo_rdusedw  : std_logic_vector(10 downto 0);
   
	
	signal event_fifo_data_in_0  : std_logic_vector(7 downto 0);  --47
   signal event_fifo_data_out_0 : std_logic_vector(7 downto 0);  --47
	signal event_fifo_wrreq_0    : std_logic;
   signal event_fifo_wrfull_0   : std_logic;
   signal event_fifo_rdreq_0    : std_logic;
   signal event_fifo_rdempty_0  : std_logic;
   signal event_fifo_rdusedw_0  : std_logic_vector(10 downto 0);
	
	signal event_fifo_data_in_1  : std_logic_vector(7 downto 0);  --47
   signal event_fifo_data_out_1 : std_logic_vector(7 downto 0);  --47
	signal event_fifo_wrreq_1    : std_logic;
   signal event_fifo_wrfull_1   : std_logic;
   signal event_fifo_rdreq_1    : std_logic;
   signal event_fifo_rdempty_1  : std_logic;
   signal event_fifo_rdusedw_1  : std_logic_vector(10 downto 0);
	
	signal Signal_selectWriteFifo : std_logic;
	signal Signal_selectReadFifo  : std_logic;
	signal startSendingUDP : std_logic;
	
   -- Signal triggering event collection
   signal event_trg           : std_logic;

   -- Signal triggering the new packet
	signal pkt_start         : std_logic := '0';
   -- Packet handling signals
   signal pkt_ready         : std_logic := '0';
   signal pkt_data_len      : std_logic_vector(10 downto 0);
   
   -- Timestamp counter signals
   signal timestamp	 : std_logic_vector(31 downto 0) := (others => '0');
   signal timestamp_ovf : std_logic := '0';

   -- Debugging signal
	signal cnt_a         : std_logic_vector(7 downto 0) := (others => '1');
   signal cnt_testsig   : std_logic_vector(24 downto 0) := (others => '0');
--	signal prbs				: std_logic_vector(31 downto 0) := (others => '0');
   signal start_pkt_led : std_logic;
--   signal prbs_clk_en   : std_logic;

	-- Clock for AFE
	signal AFE_Master_Clk : std_logic;
	signal AFE_Sample_Clk : std_logic;
	signal AFE_Pll_Locked : std_logic;
	signal AFE_Lsync_Signal : std_logic;
	signal testoutputAFE : std_logic;
	
	-- sensor clocks
	signal sensor_master_clk_signal : std_logic;
	signal sensor_lsync_signal : std_logic;
	signal sensor_fsync_signal : std_logic;
	signal sensor_field_signal : STD_LOGIC;
	
	signal sensor_power_enable_signal : std_logic;
	
	signal lineIdx_signal : std_logic_vector(9 downto 0) := (others => '0');
	signal pixelIdx_signal : std_logic_vector(9 downto 0) := (others => '0');
	signal imageIdx_signal : std_logic_vector(15 downto 0) := (others => '0');
	
	signal crappSignal : std_logic_vector(15 downto 0) := (others => '0');
	
	signal integration_time_signal : std_logic_vector(8 downto 0) := b"000000000";
	signal integration_time_uart_signal : std_logic_vector(7 downto 0) := x"00";
	
	signal servo_index_signal : std_logic_vector(7 downto 0) := x"00";
	signal send_ore_signal : std_logic :='0';
	signal uart_data_signal :std_logic_vector(7 downto 0) := x"00";
	
	signal hotPixelBram_Adr_signal : STD_LOGIC_VECTOR(9 DOWNTO 0) := b"0000000000";
	signal hotPixelBram_din_signal : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"00000000";
	signal hotPixelBram_dout_signal : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"00000000";
	signal hotPixelBram_we_signal : STD_LOGIC_VECTOR(0 DOWNTO 0) := b"0";
 
	signal white_PixelBram_AdrIn_signal : STD_LOGIC_VECTOR(7 DOWNTO 0) := x"00";
	signal white_PixelBram_AdrOut_signal : STD_LOGIC_VECTOR(7 DOWNTO 0) := x"00";
	signal white_PixelBram_din_signal : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"00000000";
	signal white_PixelBram_dout_signal : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"00000000";
	signal white_PixelBram_we_signal : STD_LOGIC_VECTOR(0 DOWNTO 0) := b"0";	 
		
	signal spotX_signal : std_logic_vector (9 downto 0) := b"0000000000";		
	signal spotY_signal : std_logic_vector (9 downto 0) := b"0000000000";

	signal en_16_x_baud_signal : std_logic :='0';
	signal uart_in_signal : STD_LOGIC_VECTOR (7 downto 0):= x"00";
	signal uart_in_buffer_read_signal : std_logic := '0';
	signal uart_in_data_present_signal : std_logic := '0';

	signal slow_clock_signal : std_logic;
	signal aperture_uart_signal : STD_LOGIC_VECTOR(7 DOWNTO 0) := x"00";
	
	signal white_pixel_threshold_signal : std_logic_vector(7 downto 0) := x"0A";
	
	signal serial_out_signal : std_logic;
Begin

Inst_clk_generator : Clk_Generator 
	Port map(
			-- Clock in ports
			 CLK_IN1 => sys_clk, -- 125.000MHz
			 -- Clock out ports
			 CLK_OUT1 => mii_clk_tx, -- 125.000MHz
			 CLK_OUT2 => int_clk, -- 100.000MHz
			 CLK_OUT3 => AFE_Sample_Clk, -- 4.1MHz phase 0 duty 50
			 CLK_OUT4 => AFE_Master_Clk, -- 8.2MHz phase -25 duty 50
 			 -- Status and control signals
			 RESET  => '0',
			 LOCKED => pll_locked
	);

-- 12FPS:
-- CLK_OUT3 => AFE_Sample_Clk, -- 4.1MHz phase 0 duty 50
-- CLK_OUT4 => AFE_Master_Clk, -- 8.2MHz phase -25 duty 50
--
-- 25fps:
-- CLK_OUT3 => AFE_Sample_Clk, -- 8.2MHz phase 0 duty 50
-- CLK_OUT4 => AFE_Master_Clk, -- 16.4MHz phase -37 duty 50

-- 50fps:
-- 16.4MHz phase 0
-- 16.4MHz phase -40.0

-- 88fps
-- 32.0MHz phase 0
-- 16.1MHz phase -37

-- 102fps
-- 37.037MHz
-- 18.519MHz   phase: -40.0

-- oder
-- 98fps
-- 35.714 MHz
-- 17.857 MHz


-- clk_out4 is divided in inst_AFE_clockDivider by 2
-- in case of 100fps, RCLKP and RCLKN freq is 8MHz
-- in the AFE a PLL is multipliing the RCLKP by number of channel -> 8MHz x 4channles = 32MHz Pixel rate
-- since only the high byte of the AFE output is used, AFE_Sample_Clk is also 32MHz


-- RGMII clock output
	nmii_clk_tx <= not mii_clk_tx;
Inst_tx_clk_ODDR : ODDR2
   Generic Map(
      DDR_ALIGNMENT => "C0", -- Sets output alignment to "NONE", "C0", "C1" 
      INIT => '0', -- Sets initial state of the Q output to '0' or '1'
      SRTYPE => "ASYNC") -- Specifies "SYNC" or "ASYNC" set/reset
   Port Map (
      Q => tx_clk, -- 1-bit output data
      C0 => mii_clk_tx, -- 1-bit clock input
      C1 => nmii_clk_tx, -- 1-bit clock input
      CE => '1',  -- 1-bit clock enable input
      D0 => '1',   -- 1-bit data input (associated with C0)
      D1 => '0',   -- 1-bit data input (associated with C1)
      R => '0',    -- 1-bit reset input
      S => '0'     -- 1-bit set input
   );
	
	ext_rst <= not pll_locked;

-- Reset generator
Inst_rst : rst_gen
	Generic Map(
		rst_cycles => 40000000,  -- 400000000
		rst1_cycles => 10000   --  100000
				  )
	Port Map(
		clk      => AFE_Master_Clk, --int_clk,
		ext_rst  => ext_rst,
		rst      => rst,
		rst1     => rst1
			  );
   rstn <= not rst1; -- PHY reset is active low

   -- Resample rst to tx domain
   Process(mii_clk_tx)
   Begin
      If mii_clk_tx'Event And mii_clk_tx = '1' Then
         rst_tx_d <= rst;
         rst_tx_dd <= rst_tx_d;
         rst_tx <= rst_tx_dd;
      End If;
   End Process;

   -- FIFO 
Inst_0_image_line_fifo : image_line_fifo
	Port Map	(
		wr_clk	      => mii_clk_tx, --probes_clk,
		rd_clk	      => mii_clk_tx,
		din		      => event_fifo_data_in_0,
		wr_en		      => event_fifo_wrreq_0,
		rd_en		      => event_fifo_rdreq_0,
		dout	         => event_fifo_data_out_0,
		full	         => event_fifo_wrfull_0,
		empty	         => event_fifo_rdempty_0,
      rd_data_count  => event_fifo_rdusedw_0(9 downto 0)
	         );

Inst_1_image_line_fifo : image_line_fifo
	Port Map	(
		wr_clk	      => mii_clk_tx, --probes_clk,
		rd_clk	      => mii_clk_tx,
		din		      => event_fifo_data_in_1,
		wr_en		      => event_fifo_wrreq_1,
		rd_en		      => event_fifo_rdreq_1,
		dout	         => event_fifo_data_out_1,
		full	         => event_fifo_wrfull_1,
		empty	         => event_fifo_rdempty_1,
      rd_data_count  => event_fifo_rdusedw_1(9 downto 0)
	         );
   
	event_fifo_rdusedw_0(10) <= '0';
	event_fifo_rdusedw_1(10) <= '0';


	-- multyplexing FIFO outputs
Inst_Fifo_Read_Multiplexer: Fifo_Read_Multiplexer 
	PORT MAP(
		clk => mii_clk_tx, --int_clk,
		selectReadFIFO => Signal_selectReadFifo,  
		readReq => event_fifo_rdreq,
		dataOut => event_fifo_data_out,
		empty => event_fifo_rdempty,
		rdUsedW => event_fifo_rdusedw(9 downto 0),
		readReq_0 => event_fifo_rdreq_0,
		readReq_1 => event_fifo_rdreq_1,
		dataOut_0 => event_fifo_data_out_0,
		dataOut_1 => event_fifo_data_out_1,
		empty_0 => event_fifo_rdempty_0,
		empty_1 => event_fifo_rdempty_1,
		rdUsedW_0 => event_fifo_rdusedw_0(9 downto 0),
		rdUsedW_1 => event_fifo_rdusedw_1(9 downto 0) 
	);
	
	-- multyplexing FIFO inputs
Inst_Fifo_Write_Multiplexer: Fifo_Write_Multiplexer 
	PORT MAP(
		clk => mii_clk_tx, --int_clk,
		selectWriteFIFO => Signal_selectWriteFifo,
		writeReq => event_fifo_wrreq,
		full => event_fifo_wrfull,
		dataIn => event_fifo_data_in,
		writeReq_0 => event_fifo_wrreq_0,
		writeReq_1 => event_fifo_wrreq_1,
		full_0 => event_fifo_wrfull_0,
		full_1 => event_fifo_wrfull_1,
		dataIn_0 => event_fifo_data_in_0,
		dataIn_1 => event_fifo_data_in_1
	);

Inst_AFE_ctrl: AFE_ctrl 
	Generic Map(
       PixelPerLine => 640,
		 NumberOfLines => 512
              )
	PORT MAP(
		selectWriteFIFO => Signal_selectWriteFifo,
		selectReadFIFO => Signal_selectReadFifo,
		clk => mii_clk_tx, --int_clk,
		sample_clk => AFE_Sample_Clk,
		dataOut => event_fifo_data_in,
		fifo_wrReq => event_fifo_wrreq,
		fifoWriteFull => event_fifo_wrfull,
		rst => rst,
		dataIn => AFE_Data,
		startSendingUDP => startSendingUDP,
		lsync => AFE_Lsync_Signal,
		testoutput => testoutputAFE,
		lineIdx => lineIdx_signal,
		imageIdx => imageIdx_signal,
		pixelIdx => pixelIdx_signal
	);

Inst_AFE_clockDivider: AFE_clockDivider 
	PORT MAP(
		clk1 => AFE_Master_Clk,
		outputP => RCLKP,
		outputN => RCLKN
	);
	
Inst_sensor_ctrl: sensor_ctrl 
	Generic Map(
      PixelPerLine => 640,
		NumberOfLines => 512
              )
	PORT MAP(
		rst => rst,
		afe_clk => AFE_Master_Clk,
		afe_lsync => AFE_Lsync_Signal,
		sensor_master_clk => sensor_master_clk_signal,
		sensor_lsync => sensor_lsync_signal,
		sensor_fsync => sensor_fsync_signal,
		sensor_field => sensor_field_signal,
		sensor_power_enabled => sensor_power_enable_signal,
		sensor_reset => S_RST,
		lineIdx => lineIdx_signal,
		imageIdx =>  imageIdx_signal,
		integration_time => integration_time_signal
	);
	
	
	
Inst_pkt_trigger : pkt_trigger
   Generic Map(
      max_idle_time_to_send   => 1000000,
      full_pkt                => 645, -- 645
      event_number_width      => 11,
      pkt_len_width           => 11,
      bytes_per_event         => 1 --6
              )
   Port Map(
      clk         => mii_clk_tx,
      rst         => rst_tx,

      events_no   => event_fifo_rdusedw,
      events_empty => fifo_rdempty, -- Indicates there are data in event fifo, or events2data
      ext_start   => startSendingUDP, --'0',

      pkt_len     => pkt_data_len,
      pkt_send    => pkt_start,
      pkt_ready   => pkt_ready
	);

	-- Output flip-flops
	outs_i(0) <= tx_en_i;
	outs_i(1) <= tx_er_i;
	outs_i(9 downto 2) <= tx_data_i;
		
Inst_ethoutputs_phy : ethoutput 
   Generic Map(
      bit_num  => 10
              )
   Port Map (
		datain_h	 => outs_i,
		datain_l	 => outs_i,
		outclock	 => mii_clk_tx,
		dataout	 => outs_o
	);
	
	tx_data <= outs_o(9 downto 2);
	tx_en   <= outs_o(0);
	tx_er   <= outs_o(1);
  
--     -- After pkt start, chenge led state
--   start_pkt_led_process : Process (mii_clk_tx)
--   Begin
--      If mii_clk_tx'Event And mii_clk_tx = '1' Then
--         If pkt_start = '1' Then
--            start_pkt_led <= Not start_pkt_led;
--         End If;
--      End If;
--   End Process;
   
	
	
   -- UDP/Ethernet core
Inst_udp_pkt_gen : udp_pkt_gen
   Generic Map(
      IGAP_LEN          => 12,     -- 12 is minimul allowed by standard	
      DATA_LEN          => 645,--1280,   -- 160 Events @ 64B/Event + 16B Header
      HEADER_LEN        => 66,     -- 8B Preambule + 14B Eth + 20B IP + 8B UDP + 16B OurHeader = 66B 
      LENGTH_OFF        => 53,       -- Where to place length of data in bytes
      BOARD_ID_MAC_OFF  => 17,       -- Where to place board ID into MAC address
      PACKET_ID_OFF     => 55,       -- Where to place number of packet
      USR_FLG_OFF       => 61        -- Where to place user flags
	)
	Port Map(
		CLK				=> mii_clk_tx,
		RST				=> rst_tx,
		
		-- Control inouts
		START			   => pkt_start,
      READY          => pkt_ready,
		USR_FLAGS	  	=> servo_index_signal & integration_time_signal(8 downto 1),--x"cdef",
		SPOT_POS			=>  	
									  spotY_signal(7)
									& spotY_signal(6)
									& spotY_signal(5)
									& spotY_signal(4)
									& spotY_signal(3)
									& spotY_signal(2)
									& spotY_signal(1)
									& spotY_signal(0)
									& "000000" 
									& spotY_signal(9)
									& spotY_signal(8)
									& spotX_signal(7)
									& spotX_signal(6)
									& spotX_signal(5)
									& spotX_signal(4)
									& spotX_signal(3)
									& spotX_signal(2)
									& spotX_signal(1)
									& spotX_signal(0)
									& "000000" 
									& spotX_signal(9)
									& spotX_signal(8),
		BOARD_ID       => x"ab", --integration_time_signal, --x"ab",
		LENGTH		   => pkt_data_len,
		
		-- Conection to packet data FIFO
		FIFO_RDREQ		=> event_fifo_rdreq, --fifo_rdreq,
		FIFO_RDEMPTY	=> event_fifo_rdempty, --fifo_rdempty,
		FIFO_DATA		=> event_fifo_data_out, --fifo_data,
		
		-- Connection to ethernet PHY
		PHY_EN 		   => tx_en_ii,
		PHY_ER	 	   => tx_er_i,
		PHY_TX   	  	=> tx_data_ii,
      PHY_READY      => tx_ready_i
	);

   tx_data_i <= tx_data_ii;
   tx_en_i <= tx_en_ii;
   tx_ready_i <= '1';


-- Integration Time Controller
Inst_integration_time_ctrl: Integration_time_ctrl PORT MAP(
	   rst      			=> rst,
		clk 					=> en_16_x_baud_signal, -- better a slow clk     mii_clk_tx,
		up  					=> uC_button_upLeft,
		down 					=> uC_button_downLeft,
		integration_time_input => integration_time_uart_signal,
		integration_time_output => integration_time_signal
	);

-- Servo ctrl
Inst_servo_ctrl_1: Servo_ctrl PORT MAP(
		rst 					=> rst,
		clk 					=> slow_clock_signal, --mii_clk_tx,
		duty  				=> spotX_signal(9 downto 2),
		servo1_Sig 			=> servo_3
	);
	
Inst_servo_ctrl_2: Servo_ctrl PORT MAP(
		rst 					=> rst,
		clk 					=> slow_clock_signal, --mii_clk_tx,
		duty					=> spotY_signal(9 downto 2),
		servo1_Sig 			=> servo_4
	);
	
	Inst_aperture_ctrl: aperture_ctrl PORT MAP(
		rst 					=> rst,
		clk 					=> slow_clock_signal, --mii_clk_tx,
		up  					=> uC_button_upRight,
		down 					=> uC_button_downRight,
		aperture_uart 		=> aperture_uart_signal,
		servo1_Sig 			=> servo_1,
		servo_index 		=> servo_index_signal
	);

	-- SDI from C to AFE
	sclk_AFE <= sclk_uC;
	sen_AFE <= sen_uC;
	sdi_AFE <= sdi_uC;
	sdo_uC <= sdo_AFE;
	
	-- Testpads
	tp15 <= '0';--AFE_Master_Clk;--AFE_D4;
	tp14 <= testoutputAFE; --sensor_field_signal; --RCLKP;--AFE_Sample_Clk;--AFE_D3;
	tp13 <=  sensor_lsync_signal;--sensor_field_signal; --event_fifo_wrreq; --'0';--AFE_D2;
	tp12 <= sensor_fsync_signal;--AFE_D1;
	tp2 <= AFE_Lsync_Signal;--sensor_lsync_signal; --sensor_master_clk_signal; --AFE_Lsync_Signal;--AFE_Data(3);  
	
	AFE_Lsync <= AFE_Lsync_Signal;
	S_LSYNC <= sensor_lsync_signal;
	S_FSYNC <= sensor_fsync_signal;
	S_FIELD <= sensor_field_signal;
	S_CLK <= sensor_master_clk_signal;
	
	LS_ENABLE <= '1';
	

	--S_DATA <= '0';
	
	-- sensor enable indicator from C to FPGA
	-- Important!!! Hold sensor in reset while power up sequence
	sensor_power_enable_signal <= uC_sensor_power_enable_indicator;
	sensor_can_be_powered <= not rst;
	
	-- Configuration of image Sensor over SPI interface
Inst_Setup_sensor_control_register: Setup_sensor_control_register PORT MAP(
		rst => rst,
		clk => sensor_master_clk_signal,
		fsync => sensor_fsync_signal,
		data => S_DATA--servo_2
	);
	
	--servo_1 <= '0'; --sensor_lsync_signal;--pixelIdx_signal(0);
	--servo_2 <= testoutputAFE;--sensor_lsync_signal; --sensor_master_clk_signal; --sensor_lsync_signal; --testoutputAFE;--AFE_Sample_Clk;--'0';sensor_master_clk_signal;
	--servo_3 <= sensor_fsync_signal; -- AFE_Lsync_Signal; --sensor_lsync_signal;--event_fifo_data_in(7); --'0';--AFE_Data(2);--'0';
	--servo_4 <= '0';
	
	
Inst_uart_tx: uart_tx PORT MAP(
		send_ore => send_ore_signal,
		data => uart_data_signal,
		serial_out => serial_out_signal,
		clk => mii_clk_tx
	);
	
	servo_2 <= serial_out_signal; 
	
	
Inst_spot_detection: spot_detection PORT MAP(
		rst => rst,
		clk => mii_clk_tx,
		send_ore => send_ore_signal,
		data_out => uart_data_signal,
		pixelIdx => pixelIdx_signal,
		lineIdx => lineIdx_signal,
		pixelValue =>  not event_fifo_data_in, --event_fifo_data_in, --x"ff", --AFE_Data, --
		hot_wea => hotPixelBram_we_signal,
    	hot_addra => hotPixelBram_Adr_signal,
		--dina => hotPixelBram_din_signal,
		hot_douta => hotPixelBram_dout_signal,
		white_wea => white_PixelBram_we_signal,
		white_addrIn => white_PixelBram_AdrIn_signal,
		white_din => white_PixelBram_din_signal,
		white_addrOut => white_PixelBram_AdrOut_signal,
		white_dout => white_PixelBram_dout_signal,
		spotX => spotX_signal,
		spotY => spotY_signal,
		white_pixel_threshold => white_pixel_threshold_signal
	);
	
Inst_Hot_Pixel_RAM : HotPixelRAM
  PORT MAP (
    clka => mii_clk_tx,
    ena => '1',
    wea => hotPixelBram_we_signal,
    addra => hotPixelBram_Adr_signal,
    dina => hotPixelBram_din_signal,
    douta => hotPixelBram_dout_signal
  );
	
Inst_white_pixel_buffer : BRAM_dualPort
  PORT MAP (
    clka => mii_clk_tx,
    wea => white_PixelBram_we_signal,
    addra => white_PixelBram_AdrIn_signal,
    dina => white_PixelBram_din_signal,
    clkb => mii_clk_tx,
    addrb => white_PixelBram_AdrOut_signal,
    doutb => white_PixelBram_dout_signal
  );	
		
		
Inst_uart_rx: uart_rx PORT MAP(
		serial_in => uC_UART_Tx,--servo_4,
		en_16_x_baud => en_16_x_baud_signal,
		data_out => uart_in_signal,
		buffer_read => uart_in_buffer_read_signal,
		buffer_data_present => uart_in_data_present_signal,
		buffer_half_full => open,
		buffer_full => open,
		buffer_reset => '0',
		clk => mii_clk_tx
	);
	
Inst_baudrate_generator: baudrate_generator PORT MAP(
		clk => mii_clk_tx,
		en_16_x_baud => en_16_x_baud_signal
	);
	
Inst_process_uart_rx: process_uart_rx PORT MAP(
		clk => en_16_x_baud_signal,
		uart_data_in => uart_in_signal,
		uart_buffer_read => uart_in_buffer_read_signal,
		uart_data_present => uart_in_data_present_signal,
		testOut => open, --servo_3,
		integration_time_uart => integration_time_uart_signal,
		aperture_uart => aperture_uart_signal,
		pixel_threshold_uart => white_pixel_threshold_signal
	);
	
Inst_slow_clock_gen: slow_clock_gen PORT MAP(
		clk => mii_clk_tx,
		RST => rst,
		slow_clk => slow_clock_signal
	);	
	
	uC_UART_Rx <= serial_out_signal;
	
End arch;

-- -90 for LSB
-- -25 for MSB

unit Unit1;

{$mode objfpc}{$H+}

interface

uses

  Classes, EpikTimer, SysUtils, Forms, Controls, Graphics, Synaser, Dialogs,
  ExtCtrls, StdCtrls, GraphType, LCLType, LCLProc, LCLIntf, ComCtrls, blcksock,
  Unix, BaseUnix, Process,math;

const
  maxPixelRow = 512;
  maxPixelLine = 661;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckGroup1: TCheckGroup;
    PaintBox1: TPaintBox;
    ProgressBar1: TProgressBar;
    SelectDirectoryDialog1: TSelectDirectoryDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);




  private

  public

  end;
   TUDPThread= class(TThread)
  private
    //buffer:ansistring;
  //procedure ShowStatus;
  protected
    procedure Execute; override;
  public
    Constructor Create(CreateSuspended : boolean);
  end;

  TFastBitmapPixel = byte;

  TFastBitmap = class
  private

  public

  end;

 // TByteArray = array [0..maxPixelRow - 1] of byte;

var
  ETStream, ETLatz: TEpikTimer;
  Form1: TForm1;
  LiveView: boolean;
  ColorTableTCol: array[0..255] of TColor;
  c_fps,fps: longint;
  shortfps:byte;
  Frame:word;
  buffer_pgm: array[0..255] of ansistring;
  hold_buffer_pgm:ansistring='';
  UDPadress:ansistring;
  s: ansistring;

 // Immage_pgm: File;
  toSave:boolean;
  UDPThread:TUDPThread;
  booleanfps:Boolean;
 // buffer:ansistring='';


implementation

{$R *.lfm}

{ TForm1 }
(*procedure TUDPThread.ShowStatus;
// this method is executed by the mainthread and can therefore access all GUI elements.
begin
  if length(buffer)>10 then
  buffer_pgm := buffer;
end;*)
 procedure TUDPThread.Execute;
var
  sock: TUDPBlockSocket;
  bufferSock: ansistring = '';
  bufferfix: ansistring = '';
  BufferImage:byte;
  i: integer;

  //Immage_pgm2: TextFile;
  aktivframe:byte;
  beforeLine:word;
  stream: TFileStream;
  buffer: ansistring = '';
begin
  //stream := TFileStream.Create('./Output/PGM.pgm', fmOpenReadWrite);
  //stream.Position := 0;
  //RunCommandInDir('./Output','/usr/bin/bash',['-c','ffmpeg -y -r 100 -i PGM.pgm -c:v huffyuv test.avi &'],s)  ;



//  AssignFile(Immage_pgm, './Output/PGM.pgm');

 // AssignFile(Immage2_pgm, './Output/TEST.pgm');
  //rewrite(Immage2_pgm);
  if fps = 0 then
  begin



    SetLength(buffer,maxPixelLine*maxPixelRow+17+6);
    ETStream.start;

    sock := TUDPBlockSocket.Create;

    sock.bind('0.0.0.0', '8200');
    if sock.LastError <> 0 then
    begin
      writeLn('Could not connect to server.');
      halt(1);
    end;
    buffersock := 'Camera is connected';
    writeln(buffersock);
  end;

  try
  while (not Terminated) and  true do
  begin
    if fps=0 then
    begin
      buffersock := sock.RecvPacket(2000);
      bufferfix :='0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
  //  writeln(fps);
    end;

    //writeln(tmpfps);
    buffer:='P5'+#10+'# Hallo'+#10+IntToStr(maxPixelLine)+' '+IntToStr(maxPixelRow)+#10+'255'#10;
    if (byte(buffersock[659])+byte(buffersock[660])+1)=1 then
    begin
    buffer:=buffer+bufferSock ;
    end
    else
    begin
      for i:=1 to (byte(buffersock[659])*256+byte(buffersock[660])+1-1) do
      begin
        buffer:=buffer+bufferfix;
      end;

      buffer:=buffer+bufferSock;

    end;
    aktivframe:=byte(bufferSock[661]) ;
    repeat

       beforeLine:=byte(buffersock[659])*256+byte(buffersock[660])+1;
       buffersock := sock.RecvPacket(2000);       if ((beforeLine+1)=(byte(buffersock[659])*256+byte(buffersock[660])+1)) and (aktivframe=byte(buffersock[661])) then
       begin
       buffer:=buffer+bufferSock;
       end
       else
       if aktivframe=byte(buffersock[661]) then
       begin

         for i:=beforeLine+1 to  byte(buffersock[659])*256+byte(buffersock[660])+1-1 do
          begin
            buffer:=buffer+bufferfix;
          end;
          buffer:=buffer+bufferSock;
       end
       else
         if  (byte(buffersock[659])*256+byte(buffersock[660])+1)<>(maxPixelRow) then
         for i:=beforeLine+1 to  maxPixelRow do
          begin

            buffer:=buffer+bufferfix;
           // buffer:=buffer+bufferSock;
          end;
    until aktivframe<>byte(buffersock[661]);
       buffer_pgm[shortfps]:=buffer;




         //  AssignFile(Immage_pgm, './Output/PGM'+Format('%0.6d',[fps])+'.pgm');


         // if (fps mod 10)=0 then            begin
              //  rewrite(Immage_pgm);
        //  seek(Immage_pgm,1);
          //writeln(FilePos(Immage_pgm));
          //if ((fps +1) mod 25) = 0 then
          //write(Immage_pgm,buffer);
          //  writeln(FilePos(Immage_pgm));
         // closefile(Immage_pgm);
          //CloseFile(Immage_pgm);
          //writeln(length(buffer) );
         // end;
           //seek(Immage_pgm,0);
           //CloseFile(Immage_pgm);
         // flush(Immage_pgm);
       //   Flush(Immage_pgm);
          //sleep(2000);
         // writeln(Immage_pgm,'');
          //writeln(Immage2_pgm,buffer);
          //CloseFile(Immage2_pgm);


    //if (fps mod 100)=0 then writeln(fps, 'Latz  ', FloatToStr(
    //      ETStream.Elapsed), 'fpsBild ', byte(buffer[661+17]));
    //if (fps mod 100)=0 then
      //writeln('fps = ',fps,' Tiem = ',ETStream.Elapsed);
     if 512*661+17+6<> length(buffer) then writeln(length(buffer_pgm),' aktivframe ',aktivframe, ' frame ',byte(buffersock[661]));
    (*for i := 0 to maxPixelRow - 1 do
    begin
      if i = 0 then
      begin
        buffersock := sock.RecvBlock(2000);
        bufferfix :=
          '000000000000000000000PPP0000000000000000000000000000000000000000000000000000000000000000000000000000000000000UUUUU0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
        buffer:='P5'+#10+'#'+#10+IntToStr(maxPixelLine)+' '+IntToStr(maxPixelRow)+#10+'255'#10;
        buffer:=buffer+bufferSock;

      end
      else
      begin
         buffersock:= sock.RecvBlock(2000);
         buffer:=buffer+bufferSock
      end;
    end;   *)
     (* if (fps mod 100)=0 then writeln(i, 'Latz ', FloatToStr(ETLatz.Elapsed), 'Stream ', FloatToStr(
          ETStream.Elapsed));  *)

    //  ETStream.start;
     // inc(Frame);
     // AssignFile(Immage_pgm, 'PGM'+IntToStr(random(10)+1)+'.pgm');
    //   AssignFile(Immage_pgm, './Output/PGM.pgm');
      // AssignFile(Immage_pgm, './Output/PGM.pgm');
      // if (fps mod 10) = 0 then



      //writeln(Immage_pgm,buffer);
   //   writeln(PO,buffer);
   //  writeln(Po,buffer);


    //  ETStream.stop    ;
    //  if (fps mod 6) = 0 then
    //  begin
    //  FastBitmapToBitmap(buffer, bmConst);
     // writeln(fps);


      //Application.ProcessMessages;
    if LiveView then
    begin
    inc(fps);
    inc(shortfps);

    end;

 // until StopLiveView = True;
//  CloseFile(PO);
end;
  finally
   ETStream.stop;
   sock.free;
   writeln('mach ich frei')   ;
  end;
end;


constructor TUDPThread.Create(CreateSuspended : boolean);
 begin
   inherited Create(CreateSuspended);
   FreeOnTerminate:=True;
 end;

//  writeln(Immage_pgm,buffer);



procedure WritePointer(P: PDouble);
begin
  Writeln(Format('%8p', [P]));
end;

procedure TrackInsect(X_position,y_position:word);

begin
  with (Form1.PaintBox1.Canvas) do
  begin
    brush.Style:=bsclear;
    pen.Width:=5;
    pen.Color:=clblue;

    Rectangle(658-640,0,658,512);
    pen.Color:=clgreen;

    Ellipse(x_Position-10,Y_position-10,x_position+10,y_position+10);
    //Rectangle(658-640,0,658,512);
  end;
end;

procedure FastBitmapToBitmap(Frame: AnsiString; Bitmap: TBitmap);

var
  PixelPtr: PInteger;
  PixelRowPtr: PInteger;
  RawImage: TRawImage;
  BytePerPixel,spalte,zeile: integer;
  X, Y: integer;



begin

  try

    (*if ((fps mod 256) = 0) then
    begin
      writeln(fps, 'PixelPalette ', Bitmap.Palette, ' Time ',
        TimeToStr(Time), 'Latz ', FloatToStr(ETLatz.Elapsed), 'MOVE ', FloatToStr(
        ETStream.Elapsed));
      ETLatz.Clear;
      ETStream.Clear;
    end; *)
    Bitmap.BeginUpdate(False);

    try
      RawImage := Bitmap.RawImage;
      PixelRowPtr := PInteger(RawImage.Data);
      BytePerPixel := RawImage.Description.BitsPerPixel div 8;

      for Y := 0 to maxPixelRow - 1 do
      begin
        PixelPtr := PixelRowPtr;
        for X := 1 to maxPixelLine  do
        begin
          PixelPtr^ := ColorTableTCol[Byte(Frame[y*maxPixelLine+X+17+6])];// 17 is the length of the header
          Inc(PByte(PixelPtr), BytePerPixel);
        end;
        Inc(PByte(PixelRowPtr), RawImage.Description.BytesPerLine);
      end;

    finally
      Bitmap.EndUpdate(False);
    end;
    Form1.PaintBox1.Canvas.Draw(0, 0, Bitmap);
    if true then
   // TrackInsect(byte(Frame[661-648+17+6])*256+byte(Frame[661-647+17+6])+18,byte(Frame[661-646+17+6])*256+byte(Frame[661-645+17+6]));
     //  TrackInsect(308-19,498+1);
      // TrackInsect((maxPixelLine-640-3+25),byte(Frame[661-646+17+6])*256+byte(Frame[661-649+17+6]));
       spalte:=  byte(Frame[661-648+17+6])*256+byte(Frame[661-647+17+6])+19;
       zeile:=byte(Frame[661-646+17+6])*256+byte(Frame[661-645+17+6])+1;
       writeln('Spalte ',inttoHex(spalte,4));
       writeln('Zeile ',IntToHex(zeile,4));
       Writeln('EineZahl ',IntToHex(spalte,4),IntToHex(zeile,4));
       writeln('Belichtungszeit[0-255] = ',byte(Frame[661-650+17+6]));
       writeln('Blende          [0-50] = ',byte(Frame[661-649+17+6]));
       writeln(Frame[661-650+17+6]);

  finally
  end;
end;




function ColorBytetoHex(grayLevel: byte): TColor;
begin
  ColorBytetoHex := RGBToColor(grayLevel, grayLevel, grayLevel);
end;

procedure initializeCameraColors;
var
  i: byte;
begin
  for i := 0 to 255 do
  begin
    ColorTableTCol[i] := ColorBytetoHex(i);
  end;
end;

procedure pgmtomp4;
var
     Progress: TextFile;
     s: ansistring='';
     s2:shortstring;
     i:byte;

     worktodo:boolean;
     Reply,boxStyle:integer;
begin

     s2:='';
     RunCommandInDir('','/usr/bin/bash',['-c','ffmpeg -y -r 51 -start_number 255 -i ./TEST%08d.pgm -c:v libx265 -x265-params -lossless=1 ./test.mp4 &>ProgressBar.txt &'],s);
     AssignFile(Progress,Form1.SelectDirectoryDialog1.FileName + 'ProgressBar.txt');
     reset(Progress) ;
     worktoDo:=TRUE;
     while WorktoDO do
     begin
        Application.ProcessMessages;
        readln(Progress,s);
        s2:='';
        for i:=1 to 6 do
        begin
          s2:=s2+s[i];
        end;
        if (s2='frame=') then
        begin
          s2:='';
          i:=7;
          while s[i]= ' ' do
          begin
             inc(i);
          end;
          repeat
             s2:=s2+s[i];
             inc(i);
          until s[i]=' ';
            writeln(s2,' c_fps',c_fps);
            Form1.ProgressBar1.Position:=round((StrToInt(s2) / (c_fps-255))*100) ;
          //  Application.ProcessMessages;
            writeln(s,'fps', c_fps);
            writeln(fps)  ;

            //Form1.ProgressBar1.Position:=10;
          // writeln(trunc((StrToInt(s2) / fps)*100));
        end
        else
        if (s2='video:') then
           WorktoDo:=FALSE;
         writeln(s)  ;
         writeln('fpls' ,fps);
     end;
     (*BoxStyle := MB_ICONQUESTION + MB_YESNO;
     Reply:=Application.MessageBox('Delete PGM Frames', 'Clean Up', BoxStyle);
     writeln(Reply);
     if Reply = IDYES then Application.MessageBox('YES       ', 'Reply',MB_ICONINFORMATION)
      else Application.MessageBox('NO         ', 'Reply', MB_ICONHAND);
     writeln(Reply);  *)
     CloseFile(Progress);
end;



procedure StartLiveView;
const
  threshold=230; // 255 withe 0 black
var
  bmConst: TBitmap;

  lastframe:word;
  evenodd:byte;
  oldfps:longint;
  UART: TBlockSerial;
  tmp_UART: AnsiString='';
  Immage_pgm,HotCoe,HotCoePgm,HotCoeTxt: TextFile;
  FIFO_pgm,Video0_pgm:TextFile  ;
  tmp_shortfps:byte;
  streamTo:ansistring;
  checked:boolean;
  oneTime:boolean;
  i,j,tmpCol,tmpRow:longword;
  tmpBuffer:ansistring='';

begin
  OneTime:=TRUE;
 // write('ich starte');
  //dirtyInitializeUDPStream;
// UART:=TBlockSerial.Create;
  checked:=Form1.CheckBox4.Checked;
  try

  //UART.Connect('/dev/ttyS0');
  //UART.config(115200,8,'N',SB1,False,False);
 //enodd:=0;

    UDPThread := TUDPThread.Create(True)   ;
    UDPThread.Start;


    if Form1.CheckBox3.Checked then
    begin
      RunCommandinDir('./Output','/usr/bin/bash',['-c','ffmpeg -y -vaapi_device /dev/dri/renderD128 -an -r 20  -i fifo.v4l -vf "format=nv12,hwupload" -an  -c:v h264_vaapi -global_quality 25 -f v4l2 /dev/video0 &'],s);
      AssignFile(Video0_pgm, './Output/fifo.v4l');
      rewrite(Video0_pgm);


    end;
      if Form1.CheckBox2.Checked then
   begin

      if Form1.Button5.Caption = 'UDPAdress' then
      begin
         Form1.Button5.Caption:=InputBox('UDP Stream','Please type in your the client adress','127.0.0.1:1234') ;
      end;
      //RunCommandInDir('./Output','/usr/bin/bash',['-c','ffmpeg -y -r 100 -i fifo.pgm -c:v huffyuv test.avi &'],s)  ;
      StreamTo:=Form1.Button5.Caption;
      RunCommandInDir('./Output','/usr/bin/bash',['-c','ffmpeg -y  -vaapi_device /dev/dri/renderD128 -an   -i fifo.pgm -vf "format=nv12,hwupload" -an  -c:v h264_vaapi -global_quality 25 -f mpegts udp://'+StreamTo+'/?pkt_size=1316 &'],s);

      //RunCommand('/usr/bin/bash',['-c','pwd'],s)  ;
      AssignFile(Fifo_pgm, './Output/fifo.pgm');
      rewrite(Fifo_pgm);
   end;
  while fps<255 do
  begin
    random(10)
  end;

   // rewrite(Immage_pgm);;
  //  AssignFile(Immage_pgm2, './Output/PGM.pgm');

  //rewrite(Immage_pgm);
  //writeln('fps = ',fps);
  bmConst := TBitmap.Create;
  bmConst.SetSize(maxPixelLine, maxPixelRow);
  bmConst.PixelFormat := pf24bit;

  //AssignFile(Immage_pgm, './Output/PGM.pgm');
      //append(Immage_pgm);
  oldfps:=fps-1;
 // if length(buffer_pgm)>10 then
 // lastframe:=byte(buffer_pgm[661+17]);
  //if length(buffer_pgm)>10 then
     //tmp_buffer_pgm:=buffer_pgm;
  //writeln(fpGetPriority(prio_process,fpgetppid));
  while LiveView do
  begin
    tmp_shortfps:=shortfps;
    // tmp_UART:=UART.RecvString(10000);
    // writeln(tmp_UART);
     //writeln('x = ', byte(tmp_UART[5])*256+byte(tmp_UART[6]),' y = ', byte(tmp_UART[7])*256+byte(tmp_UART[8]));
    // Writeln(Form1.SelectDirectoryDialog1.FileName)    ;

     if oldfps<>fps then
      begin

            if Form1.CheckBox4.Checked then
            begin
               AssignFile(Immage_pgm,Form1.SelectDirectoryDialog1.FileName + 'TEST'+Format('%0.8d',[fps])+'.pgm');
               rewrite(Immage_pgm);
               checked:=TRUE

            end;
            if tmp_shortfps>1 then
            begin
              if (fps mod 5) <> 0 then
              begin
              if Checked then
                 write(Immage_pgm,buffer_pgm[tmp_shortfps-2])
              end
              else
              begin
               if Checked then
                  write(Immage_pgm,buffer_pgm[tmp_shortfps-2]);
               if Form1.CheckBox3.Checked then
                  write(Video0_pgm,buffer_pgm[tmp_shortfps-2]);
               if Form1.CheckBox2.Checked then
                  write(Fifo_pgm,buffer_pgm[tmp_shortfps-2]);
               if Form1.CheckBox1.Checked then
                  FastBitmapToBitmap(buffer_pgm[tmp_shortfps-2],bmConst);
              end
            end
            else
            if Checked then
            begin
               if tmp_shortfps=1 then
               begin
                  //FastBitmapToBitmap(buffer_pgm[255],bmConst);
                  write(Immage_pgm,buffer_pgm[255])
               end
               else
               begin
                  //FastBitmapToBitmap(buffer_pgm[254],bmConst) ;
                    write(Immage_pgm,buffer_pgm[254]);
               end;

            end;
             if Checked then
                closefile(Immage_pgm);

            //writeln(oldfps+1,'fps ',fps);
            if Form1.CheckBox4.Checked then
            while (oldfps+1) < fps do
            begin
              AssignFile(Immage_pgm,Form1.SelectDirectoryDialog1.FileName + 'TEST'+Format('%0.8d',[oldfps+2])+'.pgm');
              //AssignFile(Immage_pgm, './Output/TEST'+Format('%0.8d',[oldfps+1])+'.pgm');
              rewrite(Immage_pgm);
              inc(tmp_shortfps);
              if tmp_shortfps>1 then
              begin
                    write(Immage_pgm,buffer_pgm[tmp_shortfps-2]);
              end
              else
              begin
                  if tmp_shortfps=1 then
                  begin
                    write(Immage_pgm,buffer_pgm[255])
                  end
                  else
                  begin
                    write(Immage_pgm,buffer_pgm[254]);
                  end
              end;
              closefile(Immage_pgm);
              inc(oldfps);
            end;
            if (Form1.CheckBox5.Checked) and (oldfps > 1000 ) and ONETime=TRUE then
            begin
               assignFile(HotCoe,Form1.SelectDirectoryDialog1.FileName+'hotPixelBramInit.coe');
                assignFile(HotCoeTxt,Form1.SelectDirectoryDialog1.FileName+'hotPixelBramInit.txt');
               assignFile(HotCoePgm,Form1.SelectDirectoryDialog1.FileName+'hotPixelBramInit.pgm');
               rewrite(HotCoe);
               rewrite(HotCoeTxt);
               rewrite(HotCoePgm);


                tmpbuffer:=buffer_pgm[200];
                writeln(HotCoePgm,tmpBuffer);
                closefile(HotCoePgm);
                // Das liegt daran das es ein Photo ist ist der Header
               for i:=(1+17+6) to (maxPixelLine*maxPixelRow+17+6) do
               begin
                  (*tmpCol:=i mod maxPixelLine ;
                  tmpRow:=ceil(i/maxPixelLine);    *)
                  if  ((((i-17-6) mod maxPixelLine) > (18+5)) and (((i-17-6) mod maxPixelLine) < 633) and (byte(tmpbuffer[i])>threshold) and ((i-17-6)>19*maxPixelLine) and ((i-17-6)<maxPixelLine*510)) then
                  begin
                  //   writeln(HotCoe,'Spalte ',inttoHex(tmpCol,4));
                  //   writeln(HotCoe,'Zeile ',IntToHex(tmpRow,4));
                    Writeln(HotCoe,IntToHex(((i-17-6) mod maxPixelLine)-18,4),  IntToHex(ceil((i-17-6)/maxPixelLine)-1,4)+',');
                    Writeln(HotCoeTxt,(((i-17-6) mod maxPixelLine)-18),';',  ceil((i-17-6)/maxPixelLine)-1);
                  end;

               end;
               //writeln(HotCoe,buffer_pgm[200]);
               closefile(HotCoe);
               closefile(HotCoeTxt);
               ONETIME:=FALSE;
            end;
        oldfps:=fps;
      end;

    Application.ProcessMessages;
  end;



  finally
   bmConst.Free;
   if Form1.CheckBox2.Checked then
   closefile(Fifo_pgm);
   if Form1.CheckBox3.Checked then
   closefile(Video0_pgm);
  // UART.Free;
  end;

end;

procedure TForm1.Button1Click(Sender: TObject);

begin
  LiveView := TRUE;
  StartLiveView;

end;

procedure TForm1.Button2Click(Sender: TObject);

begin

  LiveView := False;
  c_fps:=fps ;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  UDPThread.Terminate;
  UDPThread.FreeOnTerminate := true;

  Close();
end;

procedure TForm1.Button4Click(Sender: TObject);
begin

  if SelectDirectoryDialog1.Execute then
  begin
         Button4.Caption:=('CustumOutputFolder');
  end;

end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  Button5.Caption:=InputBox('UDP Stream','Please type in your the client adress','127.0.0.1:1234') ;

end;

procedure TForm1.Button6Click(Sender: TObject);

begin
   pgmtomp4;
   c_fps:=fps;
  // Compress:=TRUE;
end;

procedure TForm1.Button7Click(Sender: TObject);
var i:longint;
begin
  LiveView := False;
  UDPThread.Terminate;
  UDPThread.FreeOnTerminate := true;
  fps:=0;
  Frame:=0;
  fps := 0;
  shortfps:=0;
  LiveView:=True;
  StartLiveView

end;







begin
  ETStream := TEpikTimer.Create(Application);
  ETLatz := TEpikTimer.Create(Application);
  initializeCameraColors;
  Frame:=0;
  fps := 0;
  shortfps:=0;


end.
